package org.apogames.input;

import java.awt.event.MouseWheelEvent;

public final class ApoMouse extends MouseInputAdapter {
	/** Integer variable for the middle mouse button */
	public static final int MIDDLE = MouseEvent.BUTTON2;
	
	private int mouseWheelChanged;
	
	/**
	 * Constructor
	 */
	public ApoMouse() {
		this.mouseWheelChanged = 0;
	}
	
	public int getMouseWheelChanged() {
		int change = this.mouseWheelChanged;
		this.mouseWheelChanged = 0;
		return change;
	}
	
	public void mouseWheelMoved(MouseWheelEvent e) {
		this.mouseWheelChanged += e.getWheelRotation();
	}
}