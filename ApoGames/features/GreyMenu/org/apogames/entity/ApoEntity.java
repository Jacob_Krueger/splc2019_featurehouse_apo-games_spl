package org.apogames.entity;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Class of the button and player inherit and some basic stuff to
�* Provides
 * 
 * @author Dirk Aporius
 * 
 */
public class ApoEntity {
	private float x, y, startX, startY, vecX, vecY;

	private float width, height;

	private BufferedImage iBackground;

	private boolean bSelect, bVisible, bClose, bUse, bOpaque;
	
	public ApoEntity(BufferedImage iBackground, float x, float y, float width, float height) {
		this.iBackground = iBackground;
		this.startX = x;
		this.startY = y;
		this.width = width;
		this.height = height;
		this.bOpaque = true;
		this.init();
	}

	/**
	 * sets the values to their original values
	 */
	public void init() {
		this.x = this.startX;
		this.y = this.startY;
		this.bSelect = false;
		this.bVisible = true;
		this.vecX = 0.0F;
		this.vecY = 0.0F;
		this.setBUse(false);
	}

	/**
	 * returns the start X value of the entity that is always set
	 * if init is called
	 * @return returns the start X value of the entity that is always set
	 * if init is called
	 */
	public float getStartX() {
		return this.startX;
	}

	/**
	 * sets the start X value to the given value
	 * @param startX: new X start value
	 */
	public void setStartX(float startX) {
		this.startX = startX;
	}

	/**
	 * returns the start Y value of the entity that is always set
	 * if init is called
	 * @return returns the start Y value of the entity that is always set
	 * if init is called
	 */
	public float getStartY() {
		return this.startY;
	}

	/**
	 * sets the start Y value to the given value
	 * @param startX: new Y start value
	 */
	public void setStartY(float startY) {
		this.startY = startY;
	}

	/**
	 * Checking whether pixel accuracy should be checked
	 * @return TRUE, pixel accurate, not FALSE
	 */
	public boolean isBOpaque() {
		return this.bOpaque;
	}

	/**
	 * sets the boolean value to whether true or false is considered when reviewing 2 entity's transparent items
	 * @param bOpaque
	 */
	public void setBOpaque(boolean bOpaque) {
		this.bOpaque = bOpaque;
	}

	/**
	 * Returns whether the entity should be displayed or not
	 *
	 * @return returns whether the entity should be displayed or not
	 */
	public boolean isBVisible() {
		return this.bVisible;
	}

	/**
	 * Sets the visibility of the entity to the transferred value
	 * 
	 * @param bVisible
	 */
	public void setBVisible(boolean bVisible) {
		this.bVisible = bVisible;
	}

	/**
	 * indicates whether the entity has been selected or not
	 *
	 * @return TRUE if selected otherwise FALSE
	 */
	public boolean isBSelect() {
		return this.bSelect;
	}

	/**
	 * sets the boolean value whether selected or not to the one passed
	 * 
	 * @param bSelect
	 */
	public void setBSelect(boolean bSelect) {
		this.bSelect = bSelect;
	}

	/**
	 * Returns whether the JumpEntity is fixed or set by the player
	 *
	 * @return returns if the JumpEntity is fixed or set by the player
	 *         has been
	 */
	public boolean isBClose() {
		return this.bClose;
	}

	/**
	 * sets the JumpEntity whether it is fixed or not to the given value
	 * 
	 * @param close
	 */
	public void setBClose(boolean bClose) {
		this.bClose = bClose;
	}

	/**
	 * indicates whether an entity has already been used or not
	 *
	 * @return indicates whether an entity has already been used or not
	 */
	public boolean isBUse() {
		return this.bUse;
	}

	/**
	 * sets the value for the entity, whether it was used or not
	 * passed value
	 * 
	 * @param use
	 */
	public void setBUse(boolean bUse) {
		this.bUse = bUse;
	}

	/**
	 * returns the speed in the y direction
	 *
	 * @return returns the speed in y direction
	 */
	public float getVecY() {
		return this.vecY;
	}

	/**
	 * resets the speed in the y direction
	 * 
	 * @param vecX
	 */
	public void setVecY(float vecY) {
		this.vecY = vecY;
	}

	/**
	 * returns the velocity in the x direction
	 *
	 * @return returns the velocity in the x-direction
	 */
	public float getVecX() {
		return this.vecX;
	}

	/**
	 * resets the speed in the x-direction
	 * 
	 * @param vecX
	 */
	public void setVecX(float vecX) {
		this.vecX = vecX;
	}

	/**
	 * returns the picture
	 * 
	 * @return Bild
	 */
	public BufferedImage getIBackground() {
		return this.iBackground;
	}

	/**
	 * sets the picture to the transferred value
	 * 
	 * @param background
	 */
	public void setIBackground(BufferedImage background) {
		iBackground = background;
	}

	/**
	 * returns the width of the object
	 *
	 * @return width of the object
	 */
	public float getWidth() {
		return this.width;
	}

	/**
	 * sets the width of the object to the transferred value
	 * 
	 * @param width
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * returns the height of the object
	 *
	 * @return height of the object
	 */
	public float getHeight() {
		return this.height;
	}

	/**
	 * sets the height of the object to the passed value
	 * 
	 * @param height
	 */
	public void setHeight(float height) {
		this.height = height;
	}

	/**
	 * returns the x-value of the object (ie the left edge of the image
	 *
	 * @return x-value of the object
	 */
	public float getX() {
		return this.x;
	}

	/**
	 * gives the central x-value of the object (ie the middle of the head, so to speak)
	 *
	 * @return x-value of the object
	 */
	public float getXMiddle() {
		return this.x + this.width / 2;
	}

	/**
	 * sets the X value to the passed value
	 * 
	 * @param x
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * returns the y-value of the object (ie the highest point on the head)
	 *
	 * @return y value of the object
	 */
	public float getY() {
		return this.y;
	}

	/**
	 * sets the y-value of the object on the transfer
	 * 
	 * @param y
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * checks whether the transferred values are in the entity
	 *
	 * @param x: x-coordinate of the mouse
	 * @param y: y coordinate of the mouse
	 * @return: TRUE, if in there, otherwise FALSE
	 */
	public boolean intersects(float x, float y) {
		return this.intersects(x, y, 1, 1);
	}

	/**
	 * checks if the passed values (which give a rectangle) are the entity
	 * to cut
	 *
	 * @param x: X value (top left of the rectangle)
	 * @param y: Y value (top left of the rectangle)
	 * @param width: width value (how wide is the rectangle)
	 * @param height: height value (how tall is the rectangle)
	 * @return TRUE, if in there, otherwise FALSE
	 */
	public boolean intersects(float x, float y, float width, float height) {
		if (this.getRec().intersects(x, y, width, height)) {
			if (this.isBOpaque()) {
				if (this.getIBackground() != null) {
					Rectangle2D.Float myRec = (Rectangle2D.Float)this.getRec();
					Rectangle2D.Float cut = (Rectangle2D.Float)myRec.createIntersection(new Rectangle2D.Float(x, y, width, height));
					for (int i = (int)cut.y; i < (int)(cut.y + cut.height); i++) {
						for (int j = (int)cut.x; j < (int)(cut.x + cut.width); j++) {
							if ((i - this.getY() < this.getHeight()) &&
								(j - this.getX() < this.getWidth())) {
								if (this.isOpaque(this.getIBackground().getRGB((int)(j - myRec.x), (int)(i - myRec.y)))) {
									return true;
								}
							}
						}
					}
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * returns the entity's current rectangle
	 * @return returns the entity's current rectangle
	 */
	public Rectangle2D.Float getRec() {
		return new Rectangle2D.Float( this.getX(), this.getY(), this.getWidth(), this.getHeight() );
	}

	private Rectangle2D.Float getSubRec(Rectangle2D.Float source, Rectangle2D.Float part) {

		// Create rectangles
		Rectangle2D.Float sub = new Rectangle2D.Float();

		// get X - compared to the Rectangle
		if (source.x > part.x) {
			sub.x = 0;
		} else {
			sub.x = part.x - source.x;
		}

		if (source.y > part.y) {
			sub.y = 0;
		} else {
			sub.y = part.y - source.y;
		}

		sub.width = part.width;
		sub.height = part.height;

		return sub;
	}

	/**
	 * Checks whether the transferred rgb value is transparent or not
	 * @param rgb = RGB value to check
	 * @return TRUE if transparent otherwise FALSE
	 */
	private boolean isOpaque(int rgb) {

		int alpha = (rgb >> 24) & 0xff;
		// red = (rgb >> 16) & 0xff;
		// green = (rgb >> 8) & 0xff;
		// blue = (rgb ) & 0xff;

		if (alpha == 0) {
			return false;
		}

		return true;
	}

	/**
	 * Method that is always called during the update method
	 *
	 * @param delta:
	 * 		Time that has passed since the last call
	 */
	public void think(int delta) {
	}

	/**
	 * paints the object
	 * @param g
	 */
	public void render(Graphics2D g, int x, int y) {
		if ((this.getIBackground() != null) && (this.isBVisible())) {
			g.drawImage(this.iBackground, (int) (this.getX() + x), (int) (this
					.getY() + y), (int) (this.getX() + x + this.getWidth()),
					(int) (this.getY() + y + this.getHeight()), 0, 0,
					(int) this.getWidth(), (int) this.getHeight(), null);
			if (this.isBSelect()) {
				g.setColor(Color.red);
				g.drawRect((int) (this.getX() + x), (int) (this.getY() + y),
						(int) (this.getWidth() - 1),
						(int) (this.getHeight() - 1));
			}
		}
	}

}