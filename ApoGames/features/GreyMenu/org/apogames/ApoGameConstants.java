package org.apogames;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ApoGameConstants {
	public static final String PROGRAM_NAME = null;
	
	
	static {
		try {
			ApoGameConstants.ORIGINAL_FONT = Font.createFont(Font.TRUETYPE_FONT, ApoGameConstants.class.getResourceAsStream("/font/reprise.ttf") );
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final boolean[] BUTTON_GAME = new boolean[] {false, false, false, false, false, true, false, false, false, false, false, false, false};
	public static final boolean[] BUTTON_PUZZLE = new boolean[] {false, false, false, false, true, false, false, false, false, false, false, false, false};
	
	
}
