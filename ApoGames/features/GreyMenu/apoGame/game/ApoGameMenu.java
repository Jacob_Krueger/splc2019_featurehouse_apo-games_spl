package apoGame.game;

import java.awt.Font;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;
import apoGame.game.ApoGameModel;
//import apoGame.game.level.ApoGameLevel;

public class ApoGameMenu extends ApoGameModel {
	public static final String TITLE = ApoGameConstants.PROGRAM_NAME == null ? "ApoGames" : ApoGameConstants.PROGRAM_NAME;
	
	
	
	public static Font game_font;
	public static Font title_font;
	
	@Override
	public void think(int delta) {
	}
	
	@Override
	public void init() {
		this.loadFonts();
		
		//this.setUserlevels();
	}
	
	public void onResume() {
		this.loadFonts();
	}
	
	private void loadFonts() {
		
		
		ApoGameConstants.font = ApoGameConstants.ORIGINAL_FONT.deriveFont(30f);
		ApoGameMenu.title_font = ApoGameConstants.ORIGINAL_FONT.deriveFont(38f);
			
		ApoGameMenu.game_font = ApoGameConstants.ORIGINAL_FONT.deriveFont(26f);
	}	
	
	@Override
	public void mouseButtonFunction(String function) {
		if (function.equals(ApoGameMenu.QUIT)) {
			this.onBackButtonPressed();
		} 
	}
	
	public void onBackButtonPressed() {
		System.exit(0);
	}
	
	@Override
	public void render(final Graphics2D g) {
		this.getGame().drawString(g, ApoGameMenu.TITLE, 240, 45, ApoGameMenu.title_font, new float[] {1, 1, 1, 1}, new float[] {0, 0, 0, 1});
		
		int number = 1;
		if (this.getGame().getButtons() != null) {
			for(ApoButton button : this.getGame().getButtons().values()) {
				if (button.isBVisible()) {
					int x = (int)(button.getX());
					int y = (int)(button.getY());
					int width = (int)(button.getWidth());
					int height = (int)(button.getHeight());
					
					g.setColor(new Color(128, 128, 128, 255));
					g.fillRect(x, y, width, height);
					g.setColor(new Color(48f/255f, 48f/255f, 48f/255f, 1.0f));
					g.drawRect(x, y, width, height);
					
					int h = g.getFontMetrics().getHeight() - 2 * g.getFontMetrics().getDescent();
					this.getGame().drawString(g, button.getFunction(), x + width/2, y + height/2 - h/2, ApoGameMenu.font);
					
					/*for (int circle = 0; circle < 2; circle++) {
						x += circle * width;
						
						g.setColor(new Color(255, 0, 0, 255));
						if (number == 2) {
							g.setColor(new Color(0, 255, 0));
						} else if (number == 3) {
							g.setColor(new Color(0, 90, 200));
						} else if (number == 4) {
							g.setColor(new Color(255, 255, 0));
						}
						g.fillOval(x - height/2, y, height, height);

						g.setStroke(new BasicStroke(3.0f));
						g.setColor(new Color(48, 48, 48));
						g.drawOval(x - height/2, y, height, height);
						
						g.fillRect(x - 5, y + 5, 4, 15);
						g.fillRect(x + 1, y + 5, 4, 15);
						
						g.setStroke(new BasicStroke(1.0f));
					}*/
					number += 1;
				}
			}
		}
	}
	
}