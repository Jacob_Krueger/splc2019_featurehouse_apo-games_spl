package apoGame.game;

import apoGame.ApoGamePanel;

public class ApoGameButtons {
	
	public void init() {
		original();

		function = ApoGameMenu.QUIT;
		width = 200;
		height = 60;
		x = (ApoGameConstants.GAME_WIDTH /2) - (width/2);
		y = (ApoGameConstants.GAME_HEIGHT - 1) * (height - 5);
		this.game.getButtons().put(ApoGameMenu.QUIT, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameMenu.START; //ApoGameMenu.PUZZLE;
		width = 300;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = 150;
		this.game.getButtons().put(ApoGameMenu.START, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameMenu.USERLEVELS;
		width = 300;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = 150 + height * 1 + 20 * 1;
		this.game.getButtons().put(ApoGameMenu.USERLEVELS, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameMenu.EDITOR;
		width = 300;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = 150 + height * 2 + 20 * 2;
		this.game.getButtons().put(ApoGameMenu.EDITOR, new ApoButton(null, x, y, width, height, function));
		
		function = "puzzleBack"; //ApoSnakePuzzleChooser.BACK;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("puzzleBack", new ApoButton(null, x, y, width, height, function));
		
		function = "puzzleGameBack"; //ApoSnakePuzzleGame.BACK;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT - 60 - 1 * height - 20;
		this.game.getButtons().put("puzzleGameBack", new ApoButton(null, x, y, width, height, function));
		
		function = "editorBack"; //ApoSnakeEditor.BACK;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorBack", new ApoButton(null, x, y, width, height, function));
		
		function = "editorTest"; //ApoSnakeEditor.TEST;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 3 * width - 10 * 3;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorTest", new ApoButton(null, x, y, width, height, function));
		
		function = "editorUpload"; //ApoSnakeEditor.UPLOAD;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 2 * width - 10 * 2;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorUpload", new ApoButton(null, x, y, width, height, function));
		
		function = "editorXMinus"; //ApoSnakeEditor.XMINUS;
		width = 40;
		height = 40;
		x = 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorXMinus", new ApoButton(null, x, y, width, height, function));
		
		function ="editorXPlus"; // ApoSnakeEditor.XPLUS;
		width = 40;
		height = 40;
		x = 5 + 70;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorXPlus", new ApoButton(null, x, y, width, height, function));
		
		function = "editorYMinus"; //ApoSnakeEditor.YMINUS;
		width = 40;
		height = 40;
		x = 120;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorYMinus", new ApoButton(null, x, y, width, height, function));
		
		function = "editorYPlus"; //ApoSnakeEditor.YPLUS;
		width = 40;
		height = 40;
		x = 120 + 70;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put("editorYPlus", new ApoButton(null, x, y, width, height, function));
		
		for (ApoButton button : this.game.getButtons().values()) {
			button.setBOpaque(true);
		}
	}
}