package apoGame;

import org.apogames.entity.ApoButton;

import apoGame.game.ApoGameModel;

public abstract class ApoGameComponent extends ApoSubGame {
	private ApoGameModel model;
	
	@Override
	protected void update(long delta) {
		original(delta);
		int changed = this.mouse.getMouseWheelChanged();
		if (changed != 0) {
			this.mouseWheelChanged(changed);
		}
		this.think(delta);
	}
	
	public ApoGameModel getModel() {
		return this.model;
	}
	
	public boolean mouseReleased(int x, int y, boolean left) {
		if (this.buttons != null) {
			for(ApoButton button : buttons.values()) {
				if(button.getReleased(x, y) ) {
					String function = button.getFunction();
					this.setButtonFunction(function);
					if (!super.shouldRepaint()) {
						this.render();
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public void setModel(ApoGameModel model) {
		this.model = model;
	}
	
	public abstract void mouseWheelChanged(int changed);
}