package org.apogames;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import apoGame.game.ApoGameGame;

public class ApoGameConstants {
	public final static String USERLEVELS_GETPHP = "http://www.apo-games.de/apoSnake/get_level.php";
	public final static String USERLEVELS_SAVEPHP = "http://www.apo-games.de/apoSnake/save_level.php";
	
	public static final int GAME_HEIGHT = 640;
	public static final String PROGRAM_NAME = "ApoSnake";
	public static final String PROGRAM_URL = "http://www.apo-games.de/apoSnake/";
	public static final String COOKIE_NAME = "apoSnake_level";
	public static final float VERSION = 1.0f;
	
	public static boolean FPS = false;

	public static final ArrayList<String> BUTTON_GAME = new ArrayList<String>(Arrays.asList(ApoGameGame.BACK));
}