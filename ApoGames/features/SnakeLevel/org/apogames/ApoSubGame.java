package org.apogames;

public class ApoSubGame extends Thread {
	public void render() {
		if (this.screen.getBufferStrategy() != null) {
			long time = System.nanoTime();
			this.render(this.screen.getGraphics2D());
			this.screen.update();
			this.addFrame();
			this.renderTime = System.nanoTime() - time;
		} else {
			this.screen.repaint();
		}
	}

}