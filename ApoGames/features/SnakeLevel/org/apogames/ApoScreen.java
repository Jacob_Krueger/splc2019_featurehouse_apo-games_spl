package org.apogames;

import javax.swing.JPanel;
import java.awt.Graphics;

public class ApoScreen {
	private void initWindowed() {		
		this.frame = new JFrame(this.title);

		try {
			this.frame.setIconImage(ImageIO.read(this.getClass().getClassLoader().getResource("images/icon.gif")));
		} catch (IOException e) {
		} catch (Exception e) {
		}
		
		if (ApoGameConstants.BUFFER_STRATEGY) {
			Canvas window = new Canvas(this.frame.getGraphicsConfiguration().getDevice().getDefaultConfiguration());
	
			int width = this.displayConfiguration.getWidth();
			int height = this.displayConfiguration.getHeight();
			window.setPreferredSize(new Dimension(width, height));
			window.setBackground(Color.BLACK);
	
			this.addWindowListener(this.frame);
			this.frame.setResizable(false);
			this.frame.setIgnoreRepaint(true);
			this.frame.add(window);
			this.frame.pack();
			this.frame.setLocationRelativeTo(null);
			this.frame.setEnabled(true);
			this.frame.setVisible(true);
	
			window.createBufferStrategy(2);
			this.bufferStrategy = window.getBufferStrategy();
			
			this.component = window;
		} else {
			JPanel window = new JPanel() {
				private static final long serialVersionUID = 1L;

				/**
				 * is called by repaint and repaints the component
				 */
				public void paintComponent(Graphics g) {
					try {
						long time = System.nanoTime();
						ApoScreen.this.getSubGame().render((Graphics2D) g);
						ApoScreen.this.getSubGame().addFrame();
						ApoScreen.this.getSubGame().renderTime = System.nanoTime() - time;
					} catch (NullPointerException ex) {
						return;
					} catch (Exception ex) {
						return;
					}
				}
			};
			
			int width = this.displayConfiguration.getWidth();
			int height = this.displayConfiguration.getHeight();
			window.setPreferredSize(new Dimension(width, height));
			window.setBackground(Color.BLACK);
	
			this.addWindowListener(this.frame);
			this.frame.setResizable(false);
			this.frame.setIgnoreRepaint(false);
			this.frame.add(window);
			this.frame.pack();
			this.frame.setLocationRelativeTo(null);
			this.frame.setEnabled(true);
			this.frame.setVisible(true);
			
			this.component = window;
		}
	}
	/**
	 * repaint the frame if it is allowed to repaint
	 */
	public void repaint() {
		if (this.frame != null) {
			this.frame.repaint();
		}
	}

}