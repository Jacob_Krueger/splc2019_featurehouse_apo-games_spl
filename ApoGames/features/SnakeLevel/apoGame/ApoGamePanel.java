package apoGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

import apoGame.game.level.ApoGameUserLevels;
import apoGame.game.ApoGameLevelChooser;
import apoGame.game.ApoGameGame;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameUserLevels userlevels;
	private ApoGameGame game;
	private ApoGameLevelChooser levelChooser;
	
	private int think;
	
	public final ApoGameUserLevels getUserlevels() {
		return this.userlevels;
	}
	
	public void init() {
		original();
		/*if (this.userlevels == null) {
			this.userlevels = new ApoGameUserLevels(this);
			this.loadUserlevels();
		}*/
		super.setShouldRepaint(true);
		super.setShouldThink(true);
	}
	public void savePreferences() {
		if (ApoGameConstants.B_APPLET) {
			try {
				ApoHelp.saveData(new URL("http://www.apo-games.de/apoSnake/"), "apoSnake_solved", String.valueOf(this.levelChooser.getSolved()));
			} catch (MalformedURLException e) {
			} catch (Exception e) {
			}
		} else {
			//this.prop.setSolved(this.levelChooser.getSolved());
			//this.prop.writeLevel("properties");
		}
	}
	public void setGame(int level) {
		this.model = this.game;
		this.setButtonVisible(ApoGameConstants.BUTTON_GAME);
		
		model.init();
		this.game.loadLevel(level, false, null);
	}
	public int getMaxCanChoosen() {
		return this.levelChooser.getSolved();
	}
	public void solvedLevel(int level) {
		this.levelChooser.setSolved(level, true);
	}
	public void drawString(final Graphics2D g, final String s, final int x, final int y, final Font font) {
		this.drawString(g, s, x, y, font, new float[] {0, 0, 0, 1}, new float[] {1, 1, 1, 1});
	}
	public void drawString(final Graphics2D g, final String s, final int x, final int y, final Font font, float[] colorBack, float[] colorFront) {
		int w = 0;
		g.setFont(font);
		w = g.getFontMetrics().stringWidth(s);
		
		int h = g.getFontMetrics().getHeight() - 2 * g.getFontMetrics().getDescent();
		
		g.setColor(new Color(colorBack[0], colorBack[1], colorBack[2], colorBack[3]));
		g.drawString(s, x - w/2 + 1, y + 1 + h);
		g.setColor(new Color(colorFront[0], colorFront[1], colorFront[2], colorFront[3]));
		g.drawString(s, x - w/2 + 0, y + 0 + h);
	}
	public void setUserlevelsVisible() {
		if (this.model.equals(this.menu)) {
			this.menu.setUserlevels();
		}
	}
	public void think(long delta) {		
		this.think += delta;
		
		// Update / think
		// If 10 ms have passed, then think
		while (this.think >= 10) {
			this.think -= 10;
			if (this.model != null) {
				this.model.think((int)10);
			}	
		}
	}
	@Override
	public void render(Graphics2D g) {
		g.setColor(new Color(192, 192, 192));
		g.fillRect(0, 0, ApoGameConstants.GAME_WIDTH, ApoGameConstants.GAME_HEIGHT);
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (this.model != null) {
			this.model.render(g);
		}
		super.renderButtons(g);
		
		if (ApoGameConstants.FPS) {
			this.renderFPS(g);
		}
	}
	@Override
	public void keyReleased(int keyCode, char keyChar) {
		if (keyCode == KeyEvent.VK_F) {
			super.setShowFPS(!super.isShowFPS());
		}
		if (this.model != null) {
			this.model.keyButtonReleased(keyCode, keyChar);
		}
	}
	
	public void keyPressed(int keyCode, char keyCharacter) {
		if (this.model != null) {
			this.model.keyPressed(keyCode, keyCharacter);
		}
	}

	@Override
	public boolean mouseReleased(int x, int y, boolean left) {
		if (!super.mouseReleased(x, y, left)) {
			if (this.model != null) {
				this.model.mouseButtonReleased(x, y, !left);
			}
		}
		return false;
	}
	
	public void mousePressed(int x, int y, boolean left) {
		super.mousePressed(x, y, left);		
		if (this.model != null) {
			this.model.mousePressed(x, y, !left);
		}
	}
	
	public void mouseMoved(int x, int y) {
		super.mouseMoved(x, y);
		if (this.model != null) {
			this.model.mouseMoved(x, y);
		}
	}
	
	public void mouseDragged(int x, int y, boolean left) {
		super.mouseDragged(x, y, left);
		if (this.model != null) {
			this.model.mouseDragged(x, y);
		}
	}
	/*
	public final void loadUserlevels() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				ApoGamePanel.this.userlevels.loadUserlevels();
			}
		});
		t.start();
	}*/
	/*
	public void setUserlevelsVisible() {
		if (this.getModel().equals(this.menu)) {
			this.menu.setUserlevels();
		}
	}
	*/
}