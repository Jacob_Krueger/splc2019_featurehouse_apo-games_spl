package apoGame;

import org.apogames.ApoSubGame;

public abstract class ApoGameComponent extends ApoSubGame {
	/**
	 * is called when a mouse button is released and checks if it is over a button <br />
	 * and in this case calls the setButtonFunction with the unique function of the button
	 * @param x: X-value of the mouse (seen in the frame)
	 * @param y: Y-value of the mouse (seen in the frame)
	 * @param left: TRUE, left mouse button pressed, otherwise FALSE
	 */
	public boolean mouseReleased(int x, int y, boolean left) {
		if (this.buttons != null) {
			for (ApoButton button : this.buttons.values()) {
				if (button.getReleased(x, y)) {
					String function = button.getFunction();
					this.setButtonFunction(function);
					if (!super.shouldRepaint()) {
						this.render();
					}
					return true;
				}
			}
		}
		return false;
	}
}