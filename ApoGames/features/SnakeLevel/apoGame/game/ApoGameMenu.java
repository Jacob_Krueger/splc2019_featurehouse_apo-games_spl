package apoGame.game;

import java.awt.Font;

import apoGame.game.level.ApoGameLevel;
import org.apogames.ApoGameConstants;

public class ApoGameMenu extends ApoGameModel {
	public static Font font;
	public static Font game_font;
	public static Font title_font;
	
	public void setUserlevels() {
		/*this.getGame().getButtons().get(ApoGameMenu.USERLEVELS).setBVisible(true);
		if (ApoGameLevel.editorLevels == null) {
			this.getGame().getButtons()[2].setBVisible(false);
		}*/
	}
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.START)) {
			this.getGame().setLevelChooser();
		}
	}
	
	public void init() {
		this.loadFonts();
		original();
	}
	
	private void loadFonts() {
		
		
		ApoGameMenu.font = ApoGameConstants.ORIGINAL_FONT.deriveFont(30f);
		ApoGameMenu.title_font = ApoGameConstants.ORIGINAL_FONT.deriveFont(38f);
			
		ApoGameMenu.game_font = ApoGameConstants.ORIGINAL_FONT.deriveFont(26f);
	}
}