package apoGame.game;

import org.apogames.entity.ApoButton;

import apoGame.game.ApoGameGame;

public class ApoGameButtons {
	public void init() {
		original();
		
		function = ApoGameGame.BACK;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT - 60 - 1 * height - 20;
		this.game.getButtons().put(ApoGameGame.BACK, new ApoButton(null, x, y, width, height, function));
	}
}