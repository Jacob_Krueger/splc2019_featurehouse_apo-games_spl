package apoGame.game;

import apoGame.game.level.ApoGameLevel;
import apoGame.entity.ApoLevelChooserButton;

public class ApoGameLevelChooser extends ApoGameModel {
	private int solved = 0;
	private ApoLevelChooserButton[] levels;
	private int curShow = 0;	
	private boolean bHandCursor;
	
	public final int getSolved() {
		return this.solved;
	}

	public final void setSolved(int solved, boolean bSave) {
		if (this.solved < solved) {
			this.solved = solved;
			if (this.solved > ApoGameLevel.MAX_LEVELS - 1) {
				this.solved = ApoGameLevel.MAX_LEVELS - 1;
			}
			if (this.solved < this.levels.length) {
				for (int i = 0; i < this.solved && i < this.levels.length; i++) {
					this.levels[i].setSolved(true);
				}
			}
			if (bSave) {
				this.getGame().savePreferences();
			}
		}
	}
	
	private void loadLevels() {
		this.curShow = 0;		
		this.bHandCursor = false;
		
		if (this.levels == null) {
			this.levels = new ApoLevelChooserButton[ApoGameLevel.MAX_LEVELS];
			
			int xPos = 20;
			int yPos = 50;
			int radius = 70;
			int add = 20;
			int curLevel = 0;
			for (int y = 0; y < 6; y++) {
				for (int x = 0; x < 5; x++) {
					this.levels[curLevel] = new ApoLevelChooserButton(null, xPos, yPos, radius, radius, String.valueOf(curLevel + 1));
					
					xPos += radius + add;
					curLevel += 1;
					if (curLevel >= this.levels.length) {
						break;
					}
				}
				xPos = 20;
				yPos += radius + add;
				if (curLevel >= this.levels.length) {
					break;
				}
				if (curLevel % 30 == 0) {
					yPos = 70;
				}
			}
			this.setSolved(0, true);
		}
	}
}