package org.apogames.help;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apogames.ApoGameConstants;

public class ApoHighscore {
	private ArrayList<Integer> points;
	private ArrayList<String> names;
	private ArrayList<Integer> time;

	private void init() {
		this.names = new ArrayList<String>();
		this.time = new ArrayList<Integer>();
		this.points = new ArrayList<Integer>();
		original();
	}
	
	private void clear() {
		this.names.clear();
		this.time.clear();
		this.points.clear();
		original();
	}
	public ArrayList<String> getNames() {
		return this.names;
	}

	public ArrayList<Integer> getTime() {
		return this.time;
	}
	
	public ArrayList<Integer> getPoints() {
		return this.points;
	}
	@SuppressWarnings("deprecation")
	public boolean save(int points, int time, String name) {
		if (ApoGameConstants.B_ONLINE) {
			try {
				URL url;
				URLConnection urlConn;
				DataOutputStream printout;
				DataInputStream input;

				url = new URL(ApoGameConstants.HIGHSCORE_SAVEPHP);
				urlConn = url.openConnection();

				urlConn.setDoInput(true);
				urlConn.setDoOutput(true);

				urlConn.setUseCaches(false);
				urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

				printout = new DataOutputStream(urlConn.getOutputStream());
				
				String content = "points=" + URLEncoder.encode(String.valueOf(points), "UTF-8")
						+ "&time=" + URLEncoder.encode(String.valueOf(time), "UTF-8")
						+ "&name=" + URLEncoder.encode(name, "UTF-8");
				printout.writeBytes(content);
				printout.flush();
				printout.close();

				input = new DataInputStream(urlConn.getInputStream());
				@SuppressWarnings("unused")
				String str;
				while (null != ((str = input.readLine()))) {
				}
				input.close();
				return true;
			} catch (MalformedURLException me) {
				System.err.println("MalformedURLException: " + me);
				return false;
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
				return false;
			}
		}
		return false;
	}
}