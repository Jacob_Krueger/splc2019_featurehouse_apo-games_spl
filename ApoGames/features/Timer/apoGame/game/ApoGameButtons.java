package apoGame.game;

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.apogames.entity.ApoButton;

import org.apogames.ApoGameConstants;
import apoGame.ApoGameImageContainer;
import apoGame.game.ApoGameStateHighscore;

public class ApoGameButtons {
	public void init() {
		original();
		BufferedImage iButton;
		BufferedImage iButtonHighscore = ApoGameImageContainer.iButtonLeft;
		text = " < ";
		function = ApoGameStateHighscore.LEFT;
		width = iButtonHighscore.getWidth();
		height = iButtonHighscore.getHeight();
		x = 5;
		y = ApoGameConstants.GAME_HEIGHT/2 + 140;
		iButton = this.game.getImages().getButtonWithImage(width * 3, height, iButtonHighscore, text, Color.BLACK, Color.YELLOW, Color.RED, font);
		this.game.getButtons().put(ApoGameStateHighscore.LEFT, new ApoButton(iButton, x, y, width, height, function));
		
		text = " > ";
		function = ApoGameStateHighscore.RIGHT;
		width = iButtonHighscore.getWidth();
		height = iButtonHighscore.getHeight();
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT/2 + 140;
		iButton = this.game.getImages().getButtonWithImage(width * 3, height, iButtonHighscore, text, Color.BLACK, Color.YELLOW, Color.RED, font);
		this.game.getButtons().put(ApoGameStateHighscore.RIGHT, new ApoButton(iButton, x, y, width, height, function));
	}
}