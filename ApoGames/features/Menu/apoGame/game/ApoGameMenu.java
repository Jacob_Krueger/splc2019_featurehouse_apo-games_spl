package apoGame.game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import apoGame.ApoGamePanel;

public class ApoGameMenu extends ApoGameModel {
	public static final String QUIT = "quit";
	public static final String START = "start";
	
	
	public ApoGameMenu(ApoGamePanel game) {
		super(game);
	}
	
	@Override
	public void keyButtonReleased(int button, char character) {
		
	}
	@Override
	public void mouseButtonReleased(int x, int y, boolean bRight) {
		
	}
	
}