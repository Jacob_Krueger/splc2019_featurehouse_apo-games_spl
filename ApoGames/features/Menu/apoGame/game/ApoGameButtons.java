package apoGame.game;

import org.apogames.entity.ApoButton;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import org.apogames.ApoGameConstants;

import apoGame.ApoGameImages;
import apoGame.ApoGamePanel;

public class ApoGameButtons {
	private ApoGamePanel game;
	
	private Font font;
	private String text, function;
	int width, height, x, y;
	
	
	public ApoGameButtons(ApoGamePanel game) {
		this.game = game;
	}
	
	public void init() {
		this.game.setButtons(new HashMap<String, ApoButton>());
	}
}