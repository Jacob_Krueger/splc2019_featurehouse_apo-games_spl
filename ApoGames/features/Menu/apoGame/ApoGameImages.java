package apoGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.apogames.image.ApoImageFromValue;

public class ApoGameImages {
	public ApoGameImages() {
		this.image = new ApoImageFromValue();
	}
	
	public BufferedImage getButtonImage(int width, int height, String text, Color c, Color c2, Color c3, Color mouseOver, Color mousePressed, Font font, int round) {
		return this.image.getButtonImage(width, height, text, c, c2, c3, mouseOver, mousePressed, font, round);
	}
	
	
	public BufferedImage getButtonImageSimple(int width, int height, String text, Color background, Color font, Color border, Color over, Color pressed, boolean bLeft, boolean bRight, Font writeFont, int round) {
		BufferedImage iButton = this.getButtonImage(width, height, text, background, font, border, over, pressed, writeFont, round);
		Graphics2D g = (Graphics2D)iButton.getGraphics();
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.setColor(Color.BLACK);
		Stroke stroke = g.getStroke();
		Shape shape = g.getClip();
		
		for (int i = 0; i < 3; i++) {
			int startX = 6 + width * i / 3;
			int startY = 6;
			int myHeight = height - 2 * startY;
			g.setStroke(new BasicStroke(7));
			if (bLeft) {
				g.setClip(new Rectangle2D.Float(startX, startY, myHeight/2, myHeight));
				g.drawOval(startX + 3, startY + 3, myHeight - 7, myHeight - 7);
			}
			if (bRight) {
				g.setClip(new Rectangle2D.Float(width * (i + 1) / 3 - startY - myHeight/2, startY, myHeight/2, myHeight));
				g.drawOval(width * (i + 1) / 3 - startY - myHeight, startY + 3, myHeight - 7, myHeight - 7);
			}
			g.setStroke(stroke);
			g.setClip(shape);
		}
		
		g.dispose();
		return iButton;
	}
}