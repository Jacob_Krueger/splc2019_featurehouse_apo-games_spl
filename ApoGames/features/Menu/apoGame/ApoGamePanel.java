package apoGame;

import java.util.ArrayList;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

public class ApoGamePanel extends ApoGameComponent {
	
	public void setButtonVisible(ArrayList<String> bVisibile) {
		for(ApoButton button : this.getButtons().values()) {
			if(button.isBVisible()) {
				button.setBVisible(false);
			}
		}
		for(String button : bVisibile) {
			this.getButtons().get(button).setBVisible(true);
		}
		if (ApoGameConstants.B_APPLET) {
			//this.getButtons().get(0).setBVisible(false);
		}
	}
}