package org.apogames;

import java.awt.Font;
import java.awt.FontFormatException;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ApoGameConstants {
	public static Font ORIGINAL_FONT;
	public static final Font FONT_IMAGE = new Font(Font.SANS_SERIF, Font.BOLD, 25);
	public static final Font FONT_INSTRUCTIONS = new Font(Font.SANS_SERIF, Font.PLAIN, 25);
	
	static {
		try {
			ApoGameConstants.ORIGINAL_FONT = Font.createFont(Font.TRUETYPE_FONT, ApoGameConstants.class.getResourceAsStream("/font/reprise.ttf") );
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> BUTTON_MENU = new ArrayList<String>();
}