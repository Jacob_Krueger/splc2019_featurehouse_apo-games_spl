package apoGame.game;

import java.awt.Color;
import java.awt.Font;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

public class ApoGameButtons {
	
	public void init() {
		original();
		
		font = new Font(Font.SANS_SERIF, Font.BOLD, 30);
		text = "userlevels";
		function = ApoGameMenu.USERLEVELS;
		width = 250;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = ApoGameConstants.GAME_HEIGHT/2 - 70 - height;
		this.game.getButtons().put(ApoGameMenu.USERLEVELS, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));
	
		text = " < ";
		function = ApoGameGame.LEFT_SORT;
		width = 30;
		height = 30;
		x = ApoGameConstants.LEVEL_WIDTH  + 1 * 10  + 10;
		y = 200;
		this.game.getButtons().put(ApoGameGame.LEFT_SORT, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function));

		text = " > ";
		function = ApoGameGame.RIGHT_SORT;
		width = 30;
		height = 30;
		x = ApoGameConstants.GAME_WIDTH - 5 - width;
		y = 200;
		this.game.getButtons().put(ApoGameGame.RIGHT_SORT, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function));

		text = " < ";
		function = ApoGameGame.LEFT_LEVEL;
		width = 30;
		height = 30;
		x = ApoGameConstants.LEVEL_WIDTH  + 1 * 10  + 10;
		y = 335;
		this.game.getButtons().put(ApoGameGame.LEFT_LEVEL, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function));

		text = " > ";
		function = ApoGameGame.RIGHT_LEVEL;
		width = 30;
		height = 30;
		x = ApoGameConstants.GAME_WIDTH - 5 - width;
		y = 335;
		this.game.getButtons().put(ApoGameGame.RIGHT_LEVEL, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function));
	}
	
}