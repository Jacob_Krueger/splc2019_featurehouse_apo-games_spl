package apoGame.game;

public class ApoGameMenu extends ApoGameModel {
	public static final String USERLEVELS = "userLevels";
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.USERLEVELS)) {
			this.getGame().setUserGame();
		}
	}
}