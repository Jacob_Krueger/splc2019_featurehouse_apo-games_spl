package apoGame.game;

import apoGame.game.level.ApoGameUserLevels;

public class ApoGameGame extends ApoGameModel {
	private ApoGameUserLevels userLevels;
	
	public ApoGameGame(ApoGamePanel game) {
		if (this.userLevels == null) {
			this.userLevels = new ApoGameUserLevels();
		}
	}
	
	public void initUserLevels() {
		this.level = -3;
		if (this.sort < 0) {
			this.sort = ApoGameUserLevels.SORT_SOLUTION;
		}
		
		this.makeLevel(this.level);
		
		this.curLooseTime = 0;
		this.getGame().setShouldRepaint(false);
		this.solution = "";
	}
	
	public ApoGameUserLevels getUserLevels() {
		return this.userLevels;
	}
	private void setUserLevels() {
		this.userLevels.setCurLevel(this.userLevels.getCurLevel() + 1);
		this.makeLevel(this.level);
	}
	private void getUserLevels() {
		this.curLevel = this.userLevels.getLevel(this.userLevels.getCurLevel(), this.sort);
		this.getGame().getButtons().get(ApoGameGame.LEFT_SORT).setBVisible(true);
		this.getGame().getButtons().get(ApoGameGame.RIGHT_SORT).setBVisible(true);
		this.getGame().getButtons().get(ApoGameGame.LEFT_LEVEL).setBVisible(true);
		this.getGame().getButtons().get(ApoGameGame.RIGHT_LEVEL).setBVisible(true);
	}
	private void setLowerLevel() {
		this.userLevels.setCurLevel(this.userLevels.getCurLevel() - 1);
		this.makeLevel(this.level);
	}
	private void changeSort(int change) {
		original(change);
		if (this.sort < 0) {
			this.sort = ApoGameUserLevels.SORT_SOLUTION;
		} else if (this.sort > ApoGameUserLevels.SORT_SOLUTION) {
			this.sort = 0;
		}
		this.userLevels.setCurLevel(0);
		this.makeLevel(this.level);
	}
	private void changeLevel(int change) {
		original(change);
		if (this.level == -3) {
			this.userLevels.setCurLevel(this.userLevels.getCurLevel() + change);
		}
		this.makeLevel(this.level);
	}
	private void levelValue(String s) {
		s = String.valueOf(this.userLevels.getCurLevel() + 1);
	}
	private void sortString(String s) {
		s = ApoGameUserLevels.SORT_STRING[this.sort];
	}
}