package apoGame;

import org.apogames.ApoGameConstants;

import apoGame.game.ApoGameMenu;
import apoGame.game.level.ApoGameUserLevels;

public class ApoGamePanel extends ApoGameComponent {
	public void setUserGame() {
		if (this.game.getUserLevels().getLevel(0, ApoGameUserLevels.SORT_SOLUTION) != null) {
			this.model = this.game;
			
			this.setButtonVisible(ApoGameConstants.BUTTON_GAME);
			
			this.game.initUserLevels();
			
			this.render();
		} else {
			
		}
	}
	

	private void initMenuButtons() {
		original();
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.USERLEVELS);
	}
}