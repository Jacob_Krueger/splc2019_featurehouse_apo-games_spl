package apoGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.apogames.ApoGameConstants;
import org.apogames.image.ApoImageFromValue;

public class ApoGameImages {
	
	public final static BufferedImage getImageSimpleEditor(BufferedImage iSimpleImage, Color over, Color pressed) {
		BufferedImage iImage = new BufferedImage(iSimpleImage.getWidth() * 3, iSimpleImage.getHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = iImage.createGraphics();
		
		int width = iSimpleImage.getWidth();
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(new BasicStroke(5));
		
		for (int i = 0; i < 3; i++) {
			g.drawImage(iSimpleImage, i * width, 0, null);
			if (i > 0) {
				if (i == 1) {
					g.setColor(over);
				} else if (i == 2) {
					g.setColor(pressed);
				}
				g.drawOval(8 + i * width, 8, ApoGameConstants.TILE_SIZE - 16, ApoGameConstants.TILE_SIZE - 16);
			}
		}
		
		g.dispose();
		return iImage;
	}
}