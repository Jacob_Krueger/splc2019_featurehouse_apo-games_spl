package apoGame.game;

public class ApoGameMenu extends ApoGameModel {
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.EDITOR)) {
			this.getGame().setEditor(false);
		}
	}
}