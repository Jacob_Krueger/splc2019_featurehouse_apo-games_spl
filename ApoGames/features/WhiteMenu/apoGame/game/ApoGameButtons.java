package apoGame.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

import apoGame.ApoGameImages;

public class ApoGameButtons {
	
	public void init() {
			original();
			
			font = new Font(Font.SANS_SERIF, Font.BOLD, 30);
			text = "X";
			function = ApoGameMenu.QUIT;
			width = 45;
			height = 45;
			x = ApoGameConstants.GAME_WIDTH - 15 - width;
			y = ApoGameConstants.GAME_HEIGHT - 1 * height - 1 * 15;
			this.game.getButtons().put(ApoGameMenu.QUIT, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 255), x, y, width, height, function));
			
			text = "play";
			function = ApoGameMenu.START;
			width = 250;
			height = 60;
			x = ApoGameConstants.GAME_WIDTH/2 - width/2;
			y = 30;
			this.game.getButtons().put(ApoGameMenu.START, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));

			text = "level";
			function = ApoGameGame.MENU;
			width = 150;
			height = 40;
			x = ApoGameConstants.GAME_WIDTH - 1 * 15 - 1 * width;
			y = ApoGameConstants.GAME_HEIGHT - 1 * height - 1 * 5;
			this.game.getButtons().put(ApoGameGame.MENU, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));
	}
}