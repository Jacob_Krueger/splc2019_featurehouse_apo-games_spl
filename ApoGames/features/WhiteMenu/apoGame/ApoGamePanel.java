package apoGame;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.ArrayList;

import org.apogames.ApoGameConstants;
import org.apogames.ApoScreen;
// import org.apogames.sound.ApoMP3SoundHandler;
import org.apogames.entity.ApoButton;

import apoGame.ApoGameComponent;
import apoGame.game.ApoGameModel;
import apoGame.game.ApoGameMenu;
import apoGame.game.ApoGameButtons;

// import apoNotSoSimple.game.ApoNotSoSimpleButtons;
// import apoNotSoSimple.game.ApoNotSoSimpleCredits;
// import apoNotSoSimple.game.ApoGameEditor;
// import apoNotSoSimple.game.ApoGameGame;
// import apoNotSoSimple.game.ApoGameLevelChooser;
// import apoNotSoSimple.game.ApoGameOptions;
// import apoNotSoSimple.game.ApoGamePanel;


public class ApoGamePanel extends ApoGameComponent {
	
	private ApoGameModel model;
	private ApoGameButtons buttons;
	/** Help variables to find out how long it took to think and draw */
	private long thinkTime, renderTime;
	private boolean bSoundEffects = false;
	
	public void init() {
		super.init();
		super.setShowFPS(false);
		
		if (this.buttons == null) {
			this.buttons = new ApoGameButtons(this);
			this.buttons.init();
		}
		if (this.menu == null) {
			this.menu = new ApoGameMenu(this);
		}
		
		
		
		
		this.setMenu();
	}
	
	public void setMenu() {
		this.model = this.menu;
		this.initMenuButtons();
		this.setButtonVisible(ApoGameConstants.BUTTON_MENU);
		this.model.init();
		this.render();
	}

	private void initMenuButtons() {
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.START);
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.QUIT);
	}
	
	
	
	
	@Override
	public void setButtonFunction(String function) {
		if (this.model != null) {
			this.model.mouseButtonFunction(function);
			if (!function.equals(ApoGameMenu.QUIT)) {
				//this.playSound(ApoGamePanel.SOUND_BUTTON, 100);
			}
		}
	}

	@Override
	public void mouseReleased(int x, int y, boolean left) {
		super.mouseReleased(x, y, left);
		if (this.model != null) {
			this.model.mouseButtonReleased(x, y, !left);
		}
	}
	
	public void mousePressed(int x, int y, boolean left) {
		super.mousePressed(x, y, left);
		if (this.model != null) {
			this.model.mousePressed(x, y, !left);
		}
	}
	
	public void mouseMoved(int x, int y) {
		super.mouseMoved(x, y);
		if (this.model != null) {
			this.model.mouseMoved(x, y);
		}
	}
	
	public void mouseDragged(int x, int y, boolean left) {
		super.mouseDragged(x, y, left);
		if (this.model != null) {
			this.model.mouseDragged(x, y, !left);
		}
	}

	
	public void renderBackground(Graphics2D g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, ApoGameConstants.GAME_WIDTH, ApoGameConstants.GAME_HEIGHT);
	}
	
	public void think(long delta) {
		long t = System.nanoTime();
		if (this.model != null) {
			this.model.think((int)delta);
		}
		this.thinkTime = (int) (System.nanoTime() - t);
	}
	
	@Override
	public void render(Graphics2D g) {
		long t = System.nanoTime();
		if (this.model != null) {
			this.model.render(g);
		}
		super.renderButtons(g);
		this.renderFPS(g);
		this.renderTime = (int)(System.nanoTime() - t);
	}
	
	@Override
	public void keyReleased(int keyCode, char keyChar) {
		if (keyCode == KeyEvent.VK_F) {
			super.setShowFPS(!super.isShowFPS());
			this.render();
		}
		if (this.model != null) {
			this.model.keyButtonReleased(keyCode, keyChar);
		}
	}

	/**
	 * renders the ad for the FPS and think time
	 * @param g: the graphics object
	 */
	private void renderFPS(Graphics2D g) {
		if (super.isShowFPS()) {
			g.setColor(Color.red);
			g.setFont(ApoGameConstants.FONT_FPS);
			g.drawString("think time: " + this.thinkTime + " ns", 5, ApoGameConstants.GAME_HEIGHT - 45);
			g.drawString("think time: " + (this.thinkTime / 1000000) + " ms",5, ApoGameConstants.GAME_HEIGHT - 35);

			g.drawString("draw time: " + this.renderTime + " ns", 5, ApoGameConstants.GAME_HEIGHT - 25);
			g.drawString("draw time: " + (this.renderTime / 1000000) + " ms", 5, ApoGameConstants.GAME_HEIGHT - 15);
			g.drawString("FPS: " + super.getFPS(), 5, ApoGameConstants.GAME_HEIGHT - 5);
		}
	}

	
}