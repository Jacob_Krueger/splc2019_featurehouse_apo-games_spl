package apoGame;

import org.apogames.sound.ApoMP3SoundHandler;
import org.apogames.sound.ApoMP3Sound;

public class ApoGamePanel extends ApoGameComponent {
	public static final String SOUND_POP = "pop";
	public static final String SOUND_BUTTON = "button";
	public static final String SOUND_BACKGROUND = "background";
	public static final String SOUND_LOOSE = "loose";
	public static final String SOUND_WIN = "win";
	
	private ApoMP3SoundHandler sounds;
	private boolean bSoundEffects;
	
	public void init() {
		original();
		if (this.sounds == null) {
			this.sounds = new ApoMP3SoundHandler();
			this.sounds.loadSound(ApoGamePanel.SOUND_BACKGROUND, "/sounds/background.mp3");
			this.sounds.loadSound(ApoGamePanel.SOUND_POP, "/sounds/pop.mp3");
			this.sounds.loadSound(ApoGamePanel.SOUND_BUTTON, "/sounds/button.mp3");
			this.sounds.loadSound(ApoGamePanel.SOUND_LOOSE, "/sounds/loose.mp3");
			this.sounds.loadSound(ApoGamePanel.SOUND_WIN, "/sounds/win.mp3");

			this.sounds.playSound(ApoGamePanel.SOUND_BACKGROUND, true, 90);
			
			this.bSoundEffects = true;
		}
	}
	
	public void playSound(String sound, int volumen) {
		this.playSound(sound, false, volumen);
	}
	
	public void playSound(String sound, boolean bLoop, int volumen) {
		if (sound.equals(ApoGamePanel.SOUND_BACKGROUND)) {
			this.sounds.playSound(sound, bLoop, volumen);
		} else if (this.bSoundEffects) {
			this.sounds.playSound(sound, bLoop, volumen);
		}
	}
	
	public void stopSound(String sound) {
		this.sounds.stopSound(sound);
	}
	
	public void setSoundEffects(boolean soundEffects) {
		this.bSoundEffects = soundEffects;
	}
	
	public boolean isSoundEffects() {
		return this.bSoundEffects;
	}
}