package apoGame.game;

import java.awt.Color;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

public class ApoGameButtons {
	
	public void init() {
		original();
		
		text = "editor";
		function = ApoGameMenu.EDITOR;
		width = 250;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = ApoGameConstants.GAME_HEIGHT/2 + 70;
		this.game.getButtons().put(ApoGameMenu.EDITOR, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), true, false, font, 10), x, y, width, height, function));
	}
}