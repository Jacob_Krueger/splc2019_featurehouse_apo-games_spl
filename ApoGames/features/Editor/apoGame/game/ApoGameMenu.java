package apoGame.game;

public class ApoGameMenu {
	public static final String EDITOR = "editor";
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.EDITOR)) {
			this.getGame().setEditor(false);
		}
	}
}