package apoGame;

import org.apogames.ApoGameConstants;

import apoGame.game.ApoGameEditor;
import apoGame.game.ApoGameMenu;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameEditor editor;
	
	public void init() {
		original();
		if (this.editor == null) {
			this.editor = new ApoGameEditor(this);
		}
	}
	
	private void initMenuButtons() {
		original();
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.EDITOR);
	}
}