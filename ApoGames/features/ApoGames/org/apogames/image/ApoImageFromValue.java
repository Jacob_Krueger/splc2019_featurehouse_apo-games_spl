package org.apogames.image;

/**
 * creates wallpapers and buttons using the
 * Passed values ​​like size, color etc.
 * @author Dirk Aporius
 *
 */
public class ApoImageFromValue {

	private ApoImage image;
	
	public ApoImageFromValue() {
		super();
		
		this.image = new ApoImage();
	}
	
}