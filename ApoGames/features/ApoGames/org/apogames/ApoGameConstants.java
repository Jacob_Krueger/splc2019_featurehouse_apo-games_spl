package org.apogames;

import java.awt.Font;

public class ApoGameConstants{
	public static final String PROGRAM_NAME = null;
	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 480;
	public static final int VERSION = 0;
	
	public static boolean BUFFER_STRATEGY = false;
	/** Returns whether the application is an application or an applet */
	public static boolean B_APPLET = false;
	/** returns if the application is online */
	public static boolean B_ONLINE = true;
	
	public static int FPS_RENDER = 60;
	public static int FPS_THINK = 100;
	public static boolean FPS = false; 
	
	public static Font font = null;

}