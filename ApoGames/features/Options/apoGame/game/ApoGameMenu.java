package apoGame.game;

public class ApoGameMenu extends ApoGameModel {
	public static final String OPTIONS = "options";
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.OPTIONS)) {
			this.getGame().setOptions();
		}
	}
}