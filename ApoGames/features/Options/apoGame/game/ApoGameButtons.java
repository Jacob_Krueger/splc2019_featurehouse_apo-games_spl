package apoGame.game;

import java.awt.Color;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

public class ApoGameButtons {
	public void init() {
		original();
		
		text = "options";
		function = ApoGameMenu.OPTIONS;
		width = 250;
		height = 60;
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = ApoGameConstants.GAME_HEIGHT - 30 - height;
		this.game.getButtons().put(ApoGameMenu.OPTIONS, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));
		
		text = ApoGameOptions.MENU_STRING;
		function = ApoGameOptions.MENU;
		width = 150;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 1 * 15 - 1 * width;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 1 * 15;
		this.game.getButtons().put(ApoGameOptions.MENU, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));

		text = "";
		function = ApoGameOptions.MUSIC;
		width = 40;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH/2;
		y = 70;
		this.game.getButtons().put(ApoGameOptions.MUSIC, new ApoGameOptionsButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function, false));

		text = "";
		function = ApoGameOptions.SOUND;
		width = 40;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH/2;
		y = 115;
		this.game.getButtons().put(ApoGameOptions.SOUND, new ApoGameOptionsButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, false, font, 10), x, y, width, height, function, true));
	}
}