package apoGame;

import org.apogames.ApoGameConstants;

import apoGame.game.ApoGameMenu;
import apoGame.game.ApoGameOptions;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameOptions options;
	
	public void init() {
		original();
		if (this.options == null) {
			this.options = new ApoGameOptions(this);
		}
	}
	
	public void setOptions() {
		this.model = this.options;
		this.initOptionsMenu();
		this.setButtonVisible(ApoGameConstants.BUTTON_OPTIONS);
		
		this.model.init();
		
		this.render();
	}
	
	private void initMenuButtons() {
		original();
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.OPTIONS);
	}
	
	private void initOptionsMenu() {
		ApoGameConstants.BUTTON_OPTIONS.add(ApoGameOptions.MENU);
		ApoGameConstants.BUTTON_OPTIONS.add(ApoGameOptions.MUSIC);
		ApoGameConstants.BUTTON_OPTIONS.add(ApoGameOptions.SOUND);
	}
}