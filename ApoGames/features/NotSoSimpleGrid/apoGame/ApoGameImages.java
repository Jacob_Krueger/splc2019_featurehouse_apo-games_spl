package apoGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

public class ApoGameImages {

/**
	 * returns a three-part image back with passed width, height, colors, and text
	* @param width = width of the image
	* @param height = height of the image
	* @param text = text on the button
	* @param background = background color
	* @param font = text color
	* @param border = border color
	* @param over = color on mouse over it
	* @param pressed = color pressed at mouse
	* @return returns a three-part image back with passed width, height, colors, and text
	 */
	public BufferedImage getButtonImageLevelChooser(int width, int height, String text, Color background, Color font, Color border, Color over, Color pressed, Font writeFont, int round) {
		BufferedImage iButton = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(width,height,BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)iButton.getGraphics();
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Font fontOkButton = writeFont;
		g.setFont(fontOkButton);
		int h = g.getFontMetrics().getHeight();
		
		for ( int i = 0; i < 3; i++ ) {
			g.setColor( background );
			g.fillRoundRect((width)/3 * i + 1, 1, (width)/3 - 2, height - 1, round, round);
			
			g.setFont( fontOkButton );
			int w = g.getFontMetrics().stringWidth( text );
			int x = iButton.getWidth()/3;
			
			if ( i == 1 ) {
				g.setColor(over);
				g.fillRoundRect(i*x + x/2 - w/2, iButton.getHeight()/2 - h/4 + 1, w, h/2 - 2, 20, 20);
			} else if ( i == 2 ) {
				g.setColor(pressed);
				g.fillRoundRect(i*x + x/2 - w/2, iButton.getHeight()/2 - h/4 + 1, w, h/2 - 2, 20, 20);
			}
			
			g.setColor( font );
			g.drawString( text, i*x + x/2 - w/2, iButton.getHeight()/2 + h/3 );
			
			Stroke stroke = g.getStroke();
			g.setStroke(new BasicStroke(3));
			g.setColor( border );
			g.drawRoundRect( (width)/3 * i + 1, 1, (width)/3 - 3, height - 3, round, round );
			g.setStroke(stroke);
		}
		
		g.dispose();
		return iButton;
	}
}