package apoGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

import org.apogames.ApoGameConstants;

public class ApoGameImages {
	public static BufferedImage ORIGINAL_DRIVE;
	public static BufferedImage ORIGINAL_FIXED;
	public static BufferedImage ORIGINAL_UP;
	public static BufferedImage ORIGINAL_DOWN;
	public static BufferedImage ORIGINAL_LEFT;
	public static BufferedImage ORIGINAL_RIGHT;
	public static BufferedImage ORIGINAL_PLAYER;
	public static BufferedImage ORIGINAL_FINISH;
	public static BufferedImage ORIGINAL_VISIBLE_TRUE;
	public static BufferedImage ORIGINAL_VISIBLE_FALSE;
	public static BufferedImage ORIGINAL_STEP;
	public static BufferedImage ORIGINAL_STEP_FINISH;
	
	static {
		ApoGameImages.ORIGINAL_DRIVE = ApoGameImages.getOriginalImageDrive(Color.BLACK);
		ApoGameImages.ORIGINAL_FIXED = ApoGameImages.getImageSimple(Color.DARK_GRAY, 0, false, null, Color.BLACK);
		ApoGameImages.ORIGINAL_UP = ApoGameImages.getImageSimpleRaute(Color.YELLOW, ApoGameConstants.PLAYER_DIRECTION_UP, true, null, Color.BLACK);
		ApoGameImages.ORIGINAL_DOWN = ApoGameImages.getImageSimpleRaute(Color.RED, ApoGameConstants.PLAYER_DIRECTION_DOWN, true, null, Color.BLACK);
		ApoGameImages.ORIGINAL_LEFT = ApoGameImages.getImageSimpleTriangle(Color.MAGENTA, ApoGameConstants.PLAYER_DIRECTION_LEFT, true, null, Color.BLACK, false);
		ApoGameImages.ORIGINAL_RIGHT = ApoGameImages.getImageSimpleTriangle(Color.PINK, ApoGameConstants.PLAYER_DIRECTION_RIGHT, true, null, Color.BLACK, true);
		ApoGameImages.ORIGINAL_PLAYER = ApoGameImages.getImageSimple(Color.WHITE, ApoGameConstants.PLAYER_DIRECTION_CHANGEVISIBLE_UP, true, null, Color.BLACK);
		ApoGameImages.ORIGINAL_FINISH = ApoGameImages.getImageSimple(Color.GREEN, 0, false, "X", Color.BLACK);
		ApoGameImages.ORIGINAL_VISIBLE_TRUE = ApoGameImages.getImageSimpleRec(new Color(160, 120, 255), 0, false, "+", Color.BLACK);
		ApoGameImages.ORIGINAL_VISIBLE_FALSE = ApoGameImages.getImageSimpleRec(Color.ORANGE, 0, false, "-", Color.BLACK);
		ApoGameImages.ORIGINAL_STEP = ApoGameImages.getImageSimple(Color.GREEN.darker().darker(), ApoGameConstants.PLAYER_DIRECTION_CHANGEVISIBLE_LEFT, true, null, Color.BLACK);
		ApoGameImages.ORIGINAL_STEP_FINISH = ApoGameImages.getImageSimple(Color.BLUE, ApoGameConstants.PLAYER_DIRECTION_FINISH, true, null, Color.BLACK);
	}
	
	public final static BufferedImage getImageSimple(Color color, int direction, boolean arrows, String value, Color textColor) {
		BufferedImage image = new BufferedImage(ApoGameConstants.TILE_SIZE, ApoGameConstants.TILE_SIZE, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(ApoGameImages.ORIGINAL_DRIVE, 0, 0, null);
		g.setColor(color);
		g.fillOval(9, 9, ApoGameConstants.TILE_SIZE - 18, ApoGameConstants.TILE_SIZE - 18);
		
		ApoGameImages.getImageSimple(image, g, direction, arrows, value, textColor);
		
		g.dispose();
		
		return image;
	}
	public final static void getImageSimple(BufferedImage image, Graphics2D g, int direction, boolean arrows, String value, Color textColor) {
		if (arrows) {
			if (direction == ApoGameConstants.PLAYER_DIRECTION_FINISH) {
				g.setColor(textColor);
				int width = 15;
				int height = 15;
				g.fillOval(image.getWidth()/2 - width/2, image.getHeight()/2 - height/2, width, height);
			} else if (direction == ApoGameConstants.PLAYER_DIRECTION_CHANGEVISIBLE_LEFT) {
				g.setColor(textColor);
				int width = 15;
				int height = 15;
				g.setStroke(new BasicStroke(5));
				g.drawOval(image.getWidth()/2 - width/2, image.getHeight()/2 - height/2, width, height);
			} else if (direction == ApoGameConstants.PLAYER_DIRECTION_CHANGEVISIBLE_UP) {
				g.setColor(textColor);
				g.fillRoundRect(image.getWidth()/2 - 7, 12, 4, image.getHeight() - 30, 10, 10);
				g.fillRoundRect(image.getWidth()/2 + 4, 12, 4, image.getHeight() - 30, 10, 10);
			} else {
				Polygon poly = new Polygon();
				g.setColor(textColor);
				if (direction == ApoGameConstants.PLAYER_DIRECTION_UP) {
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight() - 14);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight() - 14);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight()/2 + 1);
					poly.addPoint(image.getWidth() - 15, image.getHeight()/2 + 1);
					poly.addPoint(image.getWidth()/2, image.getHeight()/2 - 9);
					poly.addPoint(15, image.getHeight()/2 + 1);
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight()/2 + 1);
				} else if (direction == ApoGameConstants.PLAYER_DIRECTION_DOWN) {
					poly.addPoint(image.getWidth()/2 - 2, 15);
					poly.addPoint(image.getWidth()/2 + 3, 15);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight()/2);
					poly.addPoint(image.getWidth() - 15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2, image.getHeight()/2 + 10);
					poly.addPoint(15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight()/2);
				} else if (direction == ApoGameConstants.PLAYER_DIRECTION_LEFT) {
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight() - 15);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight() - 15);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight()/2);
					poly.addPoint(image.getWidth() - 15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2, image.getHeight()/2 - 10);
					poly.addPoint(15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight()/2);
				} else if (direction == ApoGameConstants.PLAYER_DIRECTION_RIGHT) {
					poly.addPoint(image.getWidth()/2 - 2, 15);
					poly.addPoint(image.getWidth()/2 + 3, 15);
					poly.addPoint(image.getWidth()/2 + 3, image.getHeight()/2);
					poly.addPoint(image.getWidth() - 15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2, image.getHeight()/2 + 10);
					poly.addPoint(15, image.getHeight()/2);
					poly.addPoint(image.getWidth()/2 - 2, image.getHeight()/2);
				}
				if (poly.npoints > 0) {
					g.fillPolygon(poly);
				}
			}
		} else if (value != null) {
			g.setColor(textColor);
			g.setFont(ApoGameConstants.FONT_IMAGE);
			int w = g.getFontMetrics().stringWidth(value);
			int h = g.getFontMetrics().getHeight() - 2 * g.getFontMetrics().getDescent();
			g.drawString(value, image.getWidth()/2 - w/2, image.getHeight()/2 + h/2);
		}
	}
	public final static BufferedImage getImageSimpleRaute(Color color, int direction, boolean arrows, String value, Color textColor) {
		BufferedImage image = new BufferedImage(ApoGameConstants.TILE_SIZE, ApoGameConstants.TILE_SIZE, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Polygon p = new Polygon();

		p.addPoint(8, image.getHeight()/2);
		p.addPoint(image.getWidth()/2, 8);
		
		p.addPoint(image.getWidth() - 8, image.getHeight()/2);
		p.addPoint(image.getWidth()/2, image.getHeight() - 8);

		Stroke stroke = g.getStroke();

		g.setStroke(stroke);
		g.setColor(color);
		g.fillPolygon(p);

//		g.setStroke(new BasicStroke(2));
		g.setColor(Color.BLACK);
		g.drawPolygon(p);
		
		ApoGameImages.getImageSimple(image, g, direction, arrows, value, textColor);
		
		g.dispose();
		
		return image;
	}

	
	public final static BufferedImage getImageSimpleTriangle(Color color, int direction, boolean arrows, String value, Color textColor, boolean bDown) {
		BufferedImage image = new BufferedImage(ApoGameConstants.TILE_SIZE, ApoGameConstants.TILE_SIZE, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Polygon p = new Polygon();

		if (!bDown) {
			p.addPoint(5, image.getHeight() - 10);
			p.addPoint(image.getWidth()/2, 5);
			p.addPoint(image.getWidth() - 5, image.getHeight() - 10);
		} else {
			p.addPoint(5, 10);
			p.addPoint(image.getWidth()/2, image.getHeight() - 5);
			p.addPoint(image.getWidth() - 5, 10);			
		}
		Stroke stroke = g.getStroke();

		g.setStroke(stroke);
		g.setColor(color);
		g.fillPolygon(p);
		
//		g.setStroke(new BasicStroke(2));
		g.setColor(Color.BLACK);
		g.drawPolygon(p);

		
		ApoGameImages.getImageSimple(image, g, direction, arrows, value, textColor);
		
		g.dispose();
		
		return image;
	}
	
	public final static BufferedImage getImageSimpleRec(Color color, int direction, boolean arrows, String value, Color textColor) {
		BufferedImage image = new BufferedImage(ApoGameConstants.TILE_SIZE, ApoGameConstants.TILE_SIZE, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Stroke stroke = g.getStroke();

		g.setStroke(stroke);
		g.setColor(color);
		g.fillRoundRect(11, 11, image.getWidth() - 22, image.getHeight() - 22, 5, 5);
		

//		g.setStroke(new BasicStroke(2));
		g.setColor(Color.BLACK);
		g.drawRoundRect(11, 11, image.getWidth() - 22, image.getHeight() - 22, 5, 5);
		
		ApoGameImages.getImageSimple(image, g, direction, arrows, value, textColor);
		
		g.dispose();
		
		return image;
	}
	
	
	private final static BufferedImage getOriginalImageDrive(Color color) {
		BufferedImage image = new BufferedImage(ApoGameConstants.TILE_SIZE, ApoGameConstants.TILE_SIZE, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.BLACK);
		g.fillOval(8, 8, ApoGameConstants.TILE_SIZE - 16, ApoGameConstants.TILE_SIZE - 16);
		g.dispose();
		
		return image;
	}

}