package org.apogames;

import java.awt.Font;
import java.util.Arrays;

import apoGame.game.ApoGameGame;

public class ApoGameConstants {
	/** Level player Field */
	public static final int LEVEL_PLAYER = 2;
	/** Level false Field */
	public static final int LEVEL_FINISH = 9;
	/** Level fixed FIeld */
	public static final int LEVEL_FIXED = 1;
	/** Level free field */
	public static final int LEVEL_FREE = 0;
	/** Level up movement Field */
	public static final int LEVEL_UP = 3;
	/** Level down movement Field */
	public static final int LEVEL_DOWN = 4;
	/** Level left movement Field */
	public static final int LEVEL_LEFT = 5;
	/** Level right movement Field */
	public static final int LEVEL_RIGHT = 6;
	/** Level true Field */
	public static final int LEVEL_VISIBLE_TRUE = 7;
	/** Level false Field */
	public static final int LEVEL_VISIBLE_FALSE = 8;
		/** Level false Field */
	public static final int LEVEL_STEP = 10;
	/** Level false Field */
	public static final int LEVEL_STEP_FINISH = 11;
	public static final int TILE_SIZE = 45;
	public static final int LEVEL_WIDTH = 10 * TILE_SIZE;
	public static final int LEVEL_HEIGHT = 3 * TILE_SIZE;
	public static final int GAME_HEIGHT = 480;
	public static boolean FPS = false;
	
	
	
	public static final String PROGRAM_NAME = "NotSoSimple";
	public static final String PROGRAM_URL = "http://www.apo-games.de/apoNotSoSimple/";
	public static final String COOKIE_NAME = "apoNotSoSimple_level";
	public static final double VERSION = 0.21;	
	
	public static final Font FONT_LEVELNAME = new Font(Font.SANS_SERIF, Font.BOLD, 35);
	public static final Font FONT_DESCRIPTIONS = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
	
	public static final ArrayList<String> BUTTON_GAME = new ArrayList<String>(Arrays.asList(ApoGameGame.MENU));
}