package org.apogames.entity;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class ApoEntity {
	private float velocityX, velocityY;
	public ApoEntity(BufferedImage iBackground, float x, float y, float width, float height) {
		this.bOpaque = false;
		this.init();
	}
	
	/**
	 * sets the values ​​to their original values
	 */
	public void init() {
		original();
		this.velocityX = 0.0F;
		this.velocityY = 0.0F;
		this.setBUse(false);
	}
	
	/**
	 * returns the speed in the y-direction
	 *
	 * @return returns the speed in y direction
	 */
	public float getVelocityY() {
		return this.velocityY;
	}

	/**
	 * resets the speed in the y-direction
	 *
	 * @param velocityX
	 */
	public void setVelocityY(float velocityY) {
		this.velocityY = velocityY;
	}

	/**
	 * returns the velocity in the x-direction
	 *
	 * @return returns the speed in the x-direction
	 */
	public float getVelocityX() {
		return this.velocityX;
	}

	/**
	 * sets the speed back in the x-direction
	 *
	 * @param velocityX
	 */
	public void setVelocityX(float velocityX) {
		this.velocityX = velocityX;
	}

	/**
	 * Checks if the passed entity intersects the entity
	*
	* @param entity: entity to be checked
	* @return TRUE, if in there, otherwise FALSE
	 */
	public boolean intersects(ApoEntity entity) {
		if (this.getRec().intersects( entity.getRec())) {
			if (this.isBOpaque()) {
				return this.checkOpaqueColorCollisions( entity );
			} else {
				return true;
			}
		}
		return false;
	}
	
	
	
}