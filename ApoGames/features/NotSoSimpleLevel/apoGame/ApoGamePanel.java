package apoGame;

import org.apogames.help.ApoHelp;

import apoGame.game.level.ApoGameLevel;
import apoGame.game.ApoGameGame;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameGame game;
	
	public void setGame(int level) {
		this.model = this.game;
		this.setButtonVisible(ApoGameConstants.BUTTON_GAME);
		
		this.model.init();
		this.game.init(level);
		this.render();
	}
	
	public int getMaxLevels() {
		return this.levelChooser.getMaxLevel();
	}
	
	public void setGameWithLevel(final ApoGameLevel level) {
		this.setGame(-1);
		this.game.setLevelFromEditor(level);
	}
	
	public void setMaxLevel(final int level) {
		if (this.levelChooser.setMaxLevel(level)) {
			if (ApoGameConstants.B_APPLET) {
				try {
					ApoHelp.saveData(new URL(ApoGameConstants.PROGRAM_URL), ApoGameConstants.COOKIE_NAME, String.valueOf(level));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	

	
	public String getSolution() {
		return this.game.getSolution();
	}
}