package apoGame.game;

public class ApoGameMenu extends ApoGameModel {
public static final String START = "start";
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.START)) {
			this.getGame().setLevelChooser();
		}
	}
}