package org.apogames.image;

import java.awt.image.BufferedImage;

public class ApoImageFromValue {
	/**
	 * returns an image from the hard drive
	* @param path = string with the path
	* @param bLoad = true, for load from the jar, otherwise FALSE
	* @return returns an image, if none can be loaded, zero is returned
	 */
	public BufferedImage getImageFromPath(String path, boolean bLoad) {
		return this.image.getPic(path, bLoad);
	}	
}