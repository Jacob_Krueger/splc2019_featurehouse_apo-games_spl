package org.apogames.image;

import java.awt.MediaTracker;
import java.awt.image.BufferedImage;

public class ApoImage extends Component implements ImageObserver {
	/**
	 * Extract the image from the overlapping string and wait for it to finish loading
	* @param BildString = where is the picture
	* @param bLoad = TRUE if load externally, FALSE if image in JAR
	* @return returns the loaded image
	 */
	public BufferedImage getPic( String pic, boolean bLoad ) 
	{
		BufferedImage i;
		MediaTracker mt;
		mt = new MediaTracker(this.component);// so that everything is displayed at once
		
		try {
			i = getImage(pic, bLoad);
			if (i == null) {
				return null;
			}
		} catch (IllegalArgumentException ex) {
			return null;
		}
		mt.addImage(i, 0);

		try {
			// Wait until all pic are load,
			mt.waitForAll();
		} catch (InterruptedException e) {
			// nothing
		}
		return i;
	}
}