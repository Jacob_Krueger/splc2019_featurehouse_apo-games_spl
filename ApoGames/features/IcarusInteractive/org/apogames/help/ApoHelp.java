package org.apogames.help;

import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ApoHelp {
	/**
	 * first method to rotate an image when specifying the angle
	* only works perfectly with images whose width = height
	* @param src = pictures to be rotated
	* @param degrees = angle
	* @return rotated image
	 */
	public static BufferedImage rotateImage(BufferedImage src, double degrees) {
		AffineTransform affineTransform = AffineTransform.getRotateInstance( Math.toRadians(degrees), src.getWidth() / 2, src.getHeight() / 2);
		BufferedImage rotatedImage = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(src.getWidth(), src.getHeight(), BufferedImage.TRANSLUCENT);
		Graphics2D g = (Graphics2D)rotatedImage.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setTransform( affineTransform );
		g.drawImage( src, 0, 0, null );
		g.dispose();
		return rotatedImage;
	}
	
	/**
     * Place a String on the clipboard
	 * @param string
	 */
	public static void setClipboardContents(String string){
	    StringSelection stringSelection = new StringSelection(string);
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    clipboard.setContents(stringSelection, null);
	  }
	
	/**
	  * Get the String residing on the clipboard.
	  *
	  * @return any text found on the Clipboard; if none found, return an empty String.
	  */
	public static String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String)contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException ex){
				//highly unlikely since we are using a standard DataFlavor
			} catch (IOException ex) {
			}
		}
		return result;
	}
	
	public static double getAngleBetween2Points( float x1, float y1, float x2, float y2 ) {
		double dx = x1 - x2;
		double dy = y1 - y2;
		double angle = 0.0d;
 
		if ( dx == 0.0 ) {
			if ( dy == 0.0 ) {
				angle = 0.0;
			} else if ( dy > 0.0 ) {
				angle = Math.PI / 2.0;
			} else {
				angle = (Math.PI * 3.0) / 2.0;
			}
		}
		else if( dy == 0.0 ) {
			if( dx > 0.0 ) {
				angle = 0.0;
			} else {
				angle = Math.PI;
			}
		} else {
			if ( dx < 0.0 ) {
				angle = Math.atan( dy/dx ) + Math.PI;
			} else if( dy < 0.0 ) {
				angle = Math.atan( dy/dx ) + ( 2*Math.PI );
			} else {
				angle = Math.atan( dy/dx );
			}
		}
		return ( angle * 180 ) / Math.PI;
    }
	
	/**
	 * returns the string with the time
	 * @return returns the string with the time
	 */
	public static String getTimeToDraw(int time) {
		if (time <= 0)
			return "";
		String min = String.valueOf(time/1000/60);
		String sec = ""+((time/1000)%60);
		String msec = ""+((time/10)%100);
		if (sec.length()<2) sec = "0"+sec;
		if (msec.length()<2) msec = "0"+msec;
		String timeString = min+":"+sec+":"+msec;
		
		return timeString;
	}

}