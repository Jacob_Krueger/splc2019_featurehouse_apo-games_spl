package org.apogames.entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * this class contains an entity that runs as an animation
 * An image is transferred, which consists of several partial images
 * and every step of the game will see if enough time has passed
 * to display the next frame
 * @author Dirk Aporius
 *
 */
public class ApoAnimation extends ApoEntity {
	
	/**
	 * this variable indicates which frame is currently being displayed
	 */
	private int frame;
	/**
	 * This variable indicates how many frames there are
	 */
	private int tiles;
	/**
	 * this variable indicates which tile is currently being displayed
	 */
	private int direction;
	
	/**
	 * this variable indicates how many tiles in y-direction are there
	 */
	private int maxDirection;
	
	/**
	 * this variable indicates how much time must pass
	 * to display a new frame
	 */
	private long time;
	/**
	 * this variable indicates how much time has passed
	 * and if it is greater than the time variable,
	 * then it is reduced to its value
	 */
	private long curTime;
	
	/** this variable indicates whether the animation should be repeated */
	private boolean bLoop;
	
	/** this variable indicates whether the animation is currently running or not */
	private boolean bAnimation;
	
	/** boolean Variable that indicates whether the animation contains no transparent content */
	private final boolean bRGB;
	
	/** 2dimensional array with the pictures of the animation */
	private BufferedImage[][] images;
	
	private BufferedImage iOriginal;
	
	
	/**
	 * serves to create the object
	 * @param iAnimation: The image that consists of many sub-images for the animation
	 * @param x: X value where the animation should be displayed (left)
	 * @param y: Y value where the animation should be displayed (above)
	 * @param width: Specifies the width of a single tile
	 * @param height: indicates the height of a single tile
	 * @param tiles: indicates how many frames the picture consists of
	 * @param time: how much time must pass to display a new frame
	 */
	public ApoAnimation(BufferedImage iAnimation, float x, float y, float width, float height, int tiles, long time, int maxDirections, boolean bRGB) {
		super(iAnimation, x, y, width, height);
		
		this.setTiles(tiles);
		this.setTime(time);
		this.setBLoop(true);
		this.setBAnimation(true);
		
		this.iOriginal = iAnimation;
		
		this.bRGB = bRGB;
		this.maxDirection = maxDirections;
		this.makeImageArray();
		
		this.init();
	}
	
	public void init() {
		super.init();
		
		this.direction = 0;
		this.setFrame(0);
		this.setCurTime(0);
	}
	
	public void makeImageArray() {
		int type = BufferedImage.TYPE_INT_RGB;
		if (!this.bRGB) {
			type = BufferedImage.TYPE_INT_ARGB_PRE;
		}
		if (this.iOriginal != null) {
			int width = (int)(this.iOriginal.getWidth() / this.tiles);
			int height = (int)(this.iOriginal.getHeight() / this.maxDirection);
			this.images = new BufferedImage[this.maxDirection][this.tiles];
			if (this.iOriginal != null) {
				for (int y = 0; y < this.images.length; y++) {
					for (int x = 0; x < this.images[0].length; x++) {
						BufferedImage image = new BufferedImage(width, height, type);
						Graphics2D g = image.createGraphics();
						g.drawImage(this.iOriginal.getSubimage(x * width, y * height, width, height), 0, 0, null);
						g.dispose();
						this.images[y][x] = image;
					}
				}
			}
		}
	}
	
	/**
	 * Array with the individual frames of the images
	 * 2-Dimensional because it could contain several animations
	 * @return 2-dimensional array with the individual frames of the images
	 */
	public BufferedImage[][] getImages() {
		return this.images;
	}

	/**
	 * returns which tile should be displayed
	 * @return returns which tile should be displayed
	 */
	public int getDirection() {
		return this.direction;
	}

	/**
	 * sets the tile to be displayed to the passed value
	 * @param direction = new tiles what should be displayed
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}

	/**
	 * returns the number of tiles
	 * @return returns the number of tiles
	 */
	public int getTiles() {
		return this.tiles;
	}

	/**
	 * sets the number of tiles to the given value
	 * @param tiles
	 */
	public void setTiles(int tiles) {
		this.tiles = tiles;
	}


	/**
	* returns which frame is currently being displayed
	* @return returns which frame is currently being displayed
	*/
	public int getFrame() {
		return this.frame;
	}

	/**
	* sets the current frame to be displayed
	* on a given
	* @param frame = new frame
	*/
	public void setFrame(int frame) {
		this.frame = frame;
	}


	/**
	* returns the current time that has passed
	* @return returns the current time that has passed
	*/
	public long getCurTime() {
		return this.curTime;
	}


	/**
	 * sets the current time to the given value
	 * @param curTime = new current time
	 */
	public void setCurTime(long curTime) {
		this.curTime = curTime;
	}


	/**
	* returns how much time must elapse before a new frame is displayed
	* @return returns how much time must elapse before a new frame is displayed
	*/
	public long getTime() {
		return time;
	}


	/**
	* sets the time until a new frame is displayed to the passed value
	* @param time
	*/
	public void setTime(long time) {
		this.time = time;
	}


	/**
	* indicates that the animation should be repeated or not
	* @return TRUE, animation will be repeated, otherwise FALSE
	*/
	public boolean isBLoop() {
		return this.bLoop;
	}


	/**
	* sets the value, if the animation should be repeated, to the given value
	* @param bLoop: TRUE, animation is repeated, otherwise FALSE
	*/
	public void setBLoop(boolean bLoop) {
		this.bLoop = bLoop;
	}

	/**
	* returns if the animation is currently running
	* @return TRUE, the animation is currently running, otherwise FALSE
	*/
	public boolean isBAnimation() {
		return this.bAnimation;
	}

	/**
	* sets the variable, whether the animation is currently running on the given value
	* @param bAnimation: TRUE, the animation should start, otherwise FALSE
	*/
	public void setBAnimation(boolean bAnimation) {
		this.bAnimation = bAnimation;
	}
	
	public BufferedImage getIBackground() {
		return this.images[this.getDirection()][this.getFrame()];
	}
	
	public void setIBackground(BufferedImage background) {
		this.iOriginal = background;
	}


	/**
	* is called once every cycle
	* and see if enough time has passed,
	* to display a new frame
	* @param time: time that has passed since the last call of this function
	*/
	public void think(int time) {
		if (this.isBAnimation()) {
			this.setCurTime(this.getCurTime() + time);
			while ( this.getCurTime() >= this.getTime() ) {
				this.setCurTime(this.getCurTime() - this.getTime());
				this.setFrame(this.getFrame() + 1);
				if ( this.getFrame() >= this.getTiles() ) {
					this.setFrame(0);
					if (!this.isBLoop()) {
						this.setBAnimation(false);
					}
				}
			}
		}
	}

	public void render(Graphics2D g) {
		this.render(g, 0, 0);
	}
	
	public void render(Graphics2D g, int x, int y) {
		if (super.isBVisible()) {
			if (this.iOriginal != null) {
				g.drawImage(this.images[this.getDirection()][this.getFrame()], (int)(this.getX() + x), (int)(this.getY() + y), null);
			}
			if (super.isBSelect()) {
				g.setColor(Color.red);
				g.drawRect((int)(this.getX() - x), (int)(this.getY() - y), (int)(this.getWidth() - 1), (int)(this.getHeight() - 1));
			}
		}
	}
	
}
