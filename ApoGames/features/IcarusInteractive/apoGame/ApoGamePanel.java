package apoGame;

import apoGame.game.level.ApoGameLevel;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import apoGame.ApoGameImageContainer;
import apoGame.game.level.ApoGameLevel;
import apoGame.game.ApoGameStateHighscore;
import apoGame.game.ApoGameGame;
import apoGame.game.ApoGameState;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameLevel level;
	private ApoGameStateHighscore highscore;
	private ApoGameGame game;
	private ApoGameState state;
	
	private BufferedImage iBackground;
	
	public final ApoGameLevel getLevel() {
		return this.level;
	}
	
	public final ApoGameStateHighscore getHighscore() {
		return this.highscore;
	}
	
	public void init() {
		ApoGameImageContainer.load();
		original();
		if (this.level == null) {
			this.level = new ApoGameLevel(this);
		}
		if (this.highscore == null) {
			this.highscore = new ApoGameStateHighscore(this);
			this.highscore.init();
		}
	}
	
	public void think(long delta) {
		long t = System.nanoTime();
		if (this.model != null) {
			this.model.think((int)delta);
		}
		this.thinkTime = (int) (System.nanoTime() - t);
	}

	@Override
	public void render(Graphics2D g) {
		long t = System.nanoTime();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (this.model != null) {
			this.model.render(g);
		}
		super.renderButtons(g);
		this.renderFPS(g);
		this.renderTime = (int)(System.nanoTime() - t);
	}
	
	public void renderBackground(Graphics2D g) {
		if (this.iBackground == null) {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, ApoGameConstants.GAME_WIDTH, ApoGameConstants.GAME_HEIGHT);
		} else {
			g.drawImage(this.iBackground, 0, 0, null);
		}
	}
	public void setGame() {
		this.state = this.game;
		this.state.init();
		this.setButtonVisible(ApoGameConstants.BUTTONS_GAME);
		this.level.init();
	}
}