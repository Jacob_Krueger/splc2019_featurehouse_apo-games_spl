package apoGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

public class ApoGameImages {
	/**
	 * Method to load a picture from the jar or the file system
	 * @param path : Path to the picture
	 * @param bFile : TRUE, Load picture from the hard disk, FALSE from the JAR
	 * @return hopefully the picture
	 */
	public BufferedImage getImage(String path, boolean bFile) {
		return this.image.getImageFromPath(path, bFile);
	}
	
	public BufferedImage getButtonWithImage(int width, int height, BufferedImage iPic, String text, Color textColor, Color mousePressed, Color mouseReleased, Font font) {
		BufferedImage iButton = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(width,height,BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g = (Graphics2D)iButton.getGraphics();
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		int oneWidth = width/3;
		int widthPic = 0;
		if (iPic != null) {
			widthPic = iPic.getWidth();
		}
		g.setFont(font);
		int fontHeight = g.getFontMetrics().getHeight() - 1 * g.getFontMetrics().getDescent();
		
		for ( int i = 0; i < 3; i++ ) {
			if (iPic != null) {
				g.drawImage(iPic, oneWidth * i + oneWidth/2 - widthPic/2, height/2 - iPic.getHeight()/2, null);
			}

			int w = g.getFontMetrics().stringWidth(text);
			if (i == 1) {
				g.setColor(mousePressed);
			} else if (i == 2) {
				g.setColor(mouseReleased);
			} else {
				g.setColor(Color.WHITE);
			}
			if (text.length() > 1) {
				g.drawString(text, i * oneWidth + oneWidth/2 - w/2 + 1, height/2 + fontHeight/2 + 1);
				g.setColor(textColor);
				g.drawString(text, i * oneWidth + oneWidth/2 - w/2, height/2 + fontHeight/2);
			} else {
				Stroke stroke = g.getStroke();
				g.setStroke(new BasicStroke(3));
				g.drawRoundRect(i * oneWidth + 1, 1, oneWidth - 3, height - 3, 5, 5);
				g.setStroke(stroke);
			}
		}
		
		g.dispose();
		return iButton;
	}
}