package apoGame;

import java.awt.image.BufferedImage;

public class ApoGameImageContainer {

	public static BufferedImage iJumpFeather;
	public static BufferedImage iFeather;
	public static BufferedImage iWings;
	public static BufferedImage iGoodieArrow;
	public static BufferedImage iPlayer;
	public static BufferedImage iPlayerGood;
	public static BufferedImage iPlayerBest;
	public static BufferedImage iCloud;
	public static BufferedImage iCloudOne;
	public static BufferedImage iCloudTwo;
	public static BufferedImage iButtonBackground;
	public static BufferedImage iAnalysisBackground;
	public static BufferedImage iHighscoreBackground;
	public static BufferedImage iButtonLeft;
	public static BufferedImage iArrow;
	public static BufferedImage iEnemyOne;
	public static BufferedImage iEnemyTwo;
	public static BufferedImage iEnemyThree;
	public static BufferedImage iEnemyFour;
	public static BufferedImage iEnemyFive;
	public static BufferedImage iEnemySix;
	public static BufferedImage iEnemySeven;
	public static BufferedImage iEnemyEight;
	public static BufferedImage iEnemyNine;
	public static BufferedImage iAchievement;
	public static BufferedImage iGameHud;
	
	public static void load() {
		ApoGameImages images  = new ApoGameImages();
		if (ApoGameImageContainer.iJumpFeather == null) {
			ApoGameImageContainer.iJumpFeather = images.getImage("images/goodie_ok.png", false);
		}
		if (ApoGameImageContainer.iFeather == null) {
			ApoGameImageContainer.iFeather = images.getImage("images/goodie_good.png", false);
		}
		if (ApoGameImageContainer.iWings == null) {
			ApoGameImageContainer.iWings = images.getImage("images/goodie_best.png", false);
		}
		if (ApoGameImageContainer.iGoodieArrow == null) {
			ApoGameImageContainer.iGoodieArrow = images.getImage("images/goodie_arrow.png", false);
		}
		if (ApoGameImageContainer.iPlayer == null) {
			ApoGameImageContainer.iPlayer = images.getImage("images/icarus.png", false);
		}
		if (ApoGameImageContainer.iPlayerGood == null) {
			ApoGameImageContainer.iPlayerGood = images.getImage("images/icarus_good.png", false);
		}
		if (ApoGameImageContainer.iPlayerBest == null) {
			ApoGameImageContainer.iPlayerBest = images.getImage("images/icarus_best.png", false);
		}
		if (ApoGameImageContainer.iCloud == null) {
			ApoGameImageContainer.iCloud = images.getImage("images/wall.png", false);
		}
		if (ApoGameImageContainer.iCloudOne == null) {
			ApoGameImageContainer.iCloudOne = images.getImage("images/wall_2.png", false);
		}
		if (ApoGameImageContainer.iCloudTwo == null) {
			ApoGameImageContainer.iCloudTwo = images.getImage("images/wall_3.png", false);
		}
		if (ApoGameImageContainer.iButtonBackground == null) {
			ApoGameImageContainer.iButtonBackground = images.getImage("images/button_background.png", false);
		}
		if (ApoGameImageContainer.iAnalysisBackground == null) {
			ApoGameImageContainer.iAnalysisBackground = images.getImage("images/analysis.png", false);
		}
		if (ApoGameImageContainer.iHighscoreBackground == null) {
			ApoGameImageContainer.iHighscoreBackground = images.getImage("images/highscore.png", false);
		}
		if (ApoGameImageContainer.iButtonLeft == null) {
			ApoGameImageContainer.iButtonLeft = images.getImage("images/button_left.png", false);
		}
		if (ApoGameImageContainer.iArrow == null) {
			ApoGameImageContainer.iArrow = images.getImage("images/arrow.png", false);
		}
		if (ApoGameImageContainer.iEnemyOne == null) {
			ApoGameImageContainer.iEnemyOne = images.getImage("images/enemy_one.png", false);
		}
		if (ApoGameImageContainer.iEnemyTwo == null) {
			ApoGameImageContainer.iEnemyTwo = images.getImage("images/enemy_two.png", false);
		}
		if (ApoGameImageContainer.iEnemyThree == null) {
			ApoGameImageContainer.iEnemyThree = images.getImage("images/enemy_three.png", false);
		}
		if (ApoGameImageContainer.iEnemyFour == null) {
			ApoGameImageContainer.iEnemyFour = images.getImage("images/enemy_four.png", false);
		}
		if (ApoGameImageContainer.iEnemyFive == null) {
			ApoGameImageContainer.iEnemyFive = images.getImage("images/enemy_five.png", false);
		}
		if (ApoGameImageContainer.iEnemySix == null) {
			ApoGameImageContainer.iEnemySix = images.getImage("images/enemy_six.png", false);
		}
		if (ApoGameImageContainer.iEnemySeven == null) {
			ApoGameImageContainer.iEnemySeven = images.getImage("images/enemy_seven.png", false);
		}
		if (ApoGameImageContainer.iEnemyEight == null) {
			ApoGameImageContainer.iEnemyEight = images.getImage("images/enemy_eight.png", false);
		}
		if (ApoGameImageContainer.iEnemyNine == null) {
			ApoGameImageContainer.iEnemyNine = images.getImage("images/enemy_nine.png", false);
		}
		if (ApoGameImageContainer.iAchievement == null) {
			ApoGameImageContainer.iAchievement = images.getImage("images/achievement.png", false);
		}
		if (ApoGameImageContainer.iGameHud == null) {
			ApoGameImageContainer.iGameHud = images.getImage("images/clouds_ingame.png", false);
		}
	}
	
}
