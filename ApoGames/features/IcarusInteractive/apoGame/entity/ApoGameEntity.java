package apoGame.entity;

import java.awt.image.BufferedImage;

import apoGame.game.level.ApoGameLevel;
import org.apogames.entity.ApoEntity;

public abstract class ApoGameEntity extends ApoEntity {
	public ApoGameEntity(BufferedImage background, float x, float y, float width, float height) {
		super(background, x, y, width, height);
	}

	public void think(int delta, ApoGameLevel level) {
		super.think(delta);
		this.update(delta, level);
	}
	
	public abstract void update(int delta, ApoGameLevel level);
}