package apoGame.entity;

import java.awt.Graphics2D;

import org.apogames.help.ApoHelp;

import org.apogames.ApoGameConstants;
import apoGame.ApoGameImageContainer;
import apoGame.game.level.ApoGameLevel;

public class ApoGameArrow extends ApoGamePlatformNormal {

	public ApoGameArrow(float x, float y, float startVecX, float startVecY) {
		super(ApoGameImageContainer.iArrow, x, y, ApoGameImageContainer.iArrow.getWidth(), ApoGameImageContainer.iArrow.getHeight(), startVecX, startVecY);

		int degrees = 0;
		float newX = x + startVecX * 100;
		float newY = y + startVecY * 100;
		degrees = (int)(ApoHelp.getAngleBetween2Points(newX, newY, x, y));
		super.setIBackground(ApoHelp.rotateImage(ApoGameImageContainer.iArrow, degrees));
	}

	@Override
	public void update(int delta, ApoGameLevel level) {
		for (ApoGameEnemy entity: level.getEnemies()) {
			if (entity.isBVisible()) {
				if ((entity.getY() + ApoGameConstants.GAME_HEIGHT > this.getY()) && (entity.getY() - ApoGameConstants.GAME_HEIGHT < this.getY())) {
					if (entity.intersects(this)) {
						level.addPoint(entity.getPoints());
						level.addEnemyKill();
						entity.setBVisible(false);
						this.setBVisible(false);
						break;
					}
				}
			}
		}
		this.setX(this.getX() + this.getVelocityX() * delta);
		if (this.getX() < 0) {
			this.setBVisible(false);
		} else if (this.getX() + this.getWidth() > ApoGameConstants.GAME_WIDTH) {
			this.setBVisible(false);
		}
		this.setY(this.getY() + this.getVelocityY() * delta);
		if (this.getY() - level.getChangeY() < 0) {
			this.setBVisible(false);
		} else if (this.getY() - level.getChangeY() > ApoGameConstants.GAME_HEIGHT) {
			this.setBVisible(false);
		}
	}
	
	public void render(Graphics2D g, int changeX, int changeY) {
		if ((this.getY() - changeY >= 0) && (this.getY() - changeY <= ApoGameConstants.GAME_HEIGHT) && (this.isBVisible())) {
			if (this.getIBackground() == null) {
			} else {
				super.render(g, -changeX, changeY);
			}
		}
	}
}
