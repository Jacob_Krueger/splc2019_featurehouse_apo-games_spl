package apoGame.game;

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.apogames.entity.ApoButton;
import org.apogames.ApoGameConstants;

import apoGame.ApoGameImageContainer;

public class ApoGameButtons {
	public void init() {
		original();
		
		BufferedImage iButtonBackground = ApoGameImageContainer.iButtonBackground;
		BufferedImage iButton;
		
		text = "retry";
		function = ApoGameGame.BUTTON_RETRY;
		width = iButtonBackground.getWidth();
		height = iButtonBackground.getHeight();
		x = 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 2 * 5;
		iButton = this.game.getImages().getButtonWithImage(width * 3, height, iButtonBackground, text, Color.BLACK, Color.YELLOW, Color.RED, font);
		this.game.getButtons().put(ApoGameGame.BUTTON_RETRY, new ApoButton(iButton, x, y, width, height, function));
		
		text = "upload";
		function = ApoGameGame.BUTTON_UPLOAD;
		width = iButtonBackground.getWidth();
		height = iButtonBackground.getHeight();
		x = ApoGameConstants.GAME_WIDTH/2 - width/2;
		y = ApoGameConstants.GAME_HEIGHT/2 + 110;
		iButton = this.game.getImages().getButtonWithImage(width * 3, height, iButtonBackground, text, Color.BLACK, Color.YELLOW, Color.RED, font);
		this.game.getButtons().put(ApoGameGame.BUTTON_UPLOAD, new ApoButton(iButton, x, y, width, height, function));
		
		text = "menu";
		function = ApoGameGame.BUTTON_BACK;
		width = iButtonBackground.getWidth();
		height = iButtonBackground.getHeight();
		x = ApoGameConstants.GAME_WIDTH - width - 1 * 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 2 * 5;
		iButton = this.game.getImages().getButtonWithImage(width * 3, height, iButtonBackground, text, Color.BLACK, Color.YELLOW, Color.RED, font);
		this.game.getButtons().put(ApoGameGame.BUTTON_BACK, new ApoButton(iButton, x, y, width, height, function));
	}
}