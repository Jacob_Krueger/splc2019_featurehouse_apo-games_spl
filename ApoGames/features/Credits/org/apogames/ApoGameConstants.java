package org.apogames;

import java.util.Arrays;

import apoGame.game.ApoGameCredits;
import apoGame.game.ApoGameMenu;

public class ApoGameConstants {

	public static final ArrayList<String> BUTTON_CREDITS = new ArrayList<String>(
			Arrays.asList(ApoGameCredits.MENU)
		);

}