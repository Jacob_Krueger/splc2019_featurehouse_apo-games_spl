package apoGame.game;

public class ApoGameMenu extends ApoGameModel {
	public static final String CREDITS = "credits";
	
	@Override
	public void mouseButtonFunction(String function) {
		original(function);
		if (function.equals(ApoGameMenu.CREDITS)) {
			this.getGame().setCredits();
		}
	}
}