package apoGame;

import org.apogames.ApoGameConstants;

import apoGame.game.ApoGameCredits;
import apoGame.game.ApoGameMenu;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameCredits credits;
	
	public void init() {
		original();
		if (this.credits == null) {
			this.credits = new ApoGameCredits(this);
		}
	}
	
	public void setCredits() {
		this.model = this.credits;
		
		this.setButtonVisible(ApoGameConstants.BUTTON_CREDITS);
		
		this.model.init();
		
		this.render();
	}
	
	private void initMenuButtons() {
		original();
		ApoGameConstants.BUTTON_MENU.add(ApoGameMenu.CREDITS);
	}
}