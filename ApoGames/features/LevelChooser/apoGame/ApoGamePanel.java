package apoGame;

import java.net.MalformedURLException;
import java.net.URL;

import org.apogames.ApoGameConstants;
import org.apogames.help.ApoHelp;

import apoGame.game.ApoGameLevelChooser;
import apoGame.game.ApoGameMenu;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameLevelChooser levelChooser;
	
	public void init() {
		original();
		if (this.levelChooser == null) {
			this.levelChooser = new ApoGameLevelChooser(this);
			if (ApoGameConstants.B_APPLET) {
				String load;
				try {
					load = ApoHelp.loadData(new URL(ApoGameConstants.PROGRAM_URL), ApoGameConstants.COOKIE_NAME);
					int level = Integer.valueOf(load);
					this.levelChooser.setMaxLevel(level);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setLevelChooser() {
		this.model = this.levelChooser;
		this.setButtonVisible(ApoGameConstants.BUTTON_LEVELCHOOSER);
		this.model.init();
		this.render();
	}
	
}