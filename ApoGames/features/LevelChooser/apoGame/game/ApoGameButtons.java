package apoGame.game;

import java.awt.Color;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoButton;

public class ApoGameButtons {
	public void init() {
		original();
		
		text = "menu";
		function = ApoGameLevelChooser.MENU;
		width = 150;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 1 * 15 - 1 * width;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 1 * 15;
		this.game.getButtons().put(ApoGameLevelChooser.MENU, new ApoButton(this.game.getImages().getButtonImageSimple(width * 3, height, text, new Color(0, 0, 0, 0), Color.BLACK, Color.BLACK, new Color(255, 255, 0, 128), new Color(255, 0, 0, 128), false, true, font, 10), x, y, width, height, function));
	}
}