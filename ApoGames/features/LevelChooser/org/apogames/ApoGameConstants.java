package org.apogames;

import java.util.Arrays;
import java.util.ArrayList;

import apoGame.game.ApoGameLevelChooser;

public class ApoGameConstants {
	public static final ArrayList<String> BUTTON_LEVELCHOOSER = new ArrayList<String>(
			Arrays.asList(ApoGameLevelChooser.MENU));
}