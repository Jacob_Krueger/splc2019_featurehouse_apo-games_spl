package org.apogames;

import java.util.ArrayList;

import apoGame.game.ApoGameEditor;

public class ApoGameConstants {
	public static final ArrayList<String> BUTTON_EDITOR = new ArrayList<String>(
			Arrays.asList(ApoGameEditor.BACK,
					ApoGameEditor.TEST,
					ApoGameEditor.UPLOAD,
					ApoGameEditor.XMINUS,
					ApoGameEditor.XPLUS,
					ApoGameEditor.YMINUS,
					ApoGameEditor.YPLUS
					));
}