package apoGame.game;

import org.apogames.entity.ApoButton;

public class ApoGameButtons {
	public void init() {
		original();
		
		function = ApoGameEditor.BACK;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - width - 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.BACK, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.TEST;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 3 * width - 10 * 3;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.TEST, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.UPLOAD;
		width = 70;
		height = 40;
		x = ApoGameConstants.GAME_WIDTH - 2 * width - 10 * 2;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.UPLOAD, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.XMINUS;
		width = 40;
		height = 40;
		x = 5;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.XMINUS, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.XPLUS;
		width = 40;
		height = 40;
		x = 5 + 70;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.XPLUS, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.YMINUS;
		width = 40;
		height = 40;
		x = 120;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.YMINUS, new ApoButton(null, x, y, width, height, function));
		
		function = ApoGameEditor.YPLUS;
		width = 40;
		height = 40;
		x = 120 + 70;
		y = ApoGameConstants.GAME_HEIGHT - 1 * height - 10;
		this.game.getButtons().put(ApoGameEditor.YPLUS, new ApoButton(null, x, y, width, height, function));
	}
}