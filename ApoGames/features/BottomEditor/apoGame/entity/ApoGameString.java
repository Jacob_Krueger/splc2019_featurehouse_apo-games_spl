package apoGame.entity;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import org.apogames.ApoGameConstants;
import org.apogames.entity.ApoEntity;

public class ApoGameString {

	private String drawString;
	private int invisible;
	private int timeDecrease;
	private boolean bFade;
	private int x, y, direction, color;
	private boolean bVisible;

	public ApoGameString(float x, float y, float width, String s, boolean bWithBackground, int timeDecrease, boolean bFade) {
		this.x = (int)x;
		this.y = (int)y;
		this.direction = (int)width;
		this.color = 0;
		this.bVisible = true;
		
		this.bWithBackground = bWithBackground;
		this.drawString = s;
		this.invisible = 255;
		this.curTime = 0;
		this.timeDecrease = timeDecrease;
		this.curTime = this.timeDecrease;
		this.bFade = bFade;
	}
	
	public void think(int delta) {
		original(delta);
		if (this.curTime <= 0) {
			if (this.bFade) {
				this.curTime = this.timeDecrease;
				this.invisible -= 1;
				if (this.invisible <= 50) {
					this.invisible = 0;
					this.setVisible(false);
				}
			} else {
				this.setVisible(false);
			}
		}
	}
	
	public void render(final Graphics2D g, int changeX, int changeY) {
		if (this.isVisible()) {
			
			String s = this.drawString;
			if(ApoGameConstants.font != null)
				g.setFont(ApoGameConstants.font);
			int w = (int)(g.getFontMetrics().stringWidth(s) + 10);
			int h = g.getFontMetrics().getHeight() - 2 * g.getFontMetrics().getDescent();
			int x = (int)(this.getX() + this.getDirection()/2 - w/2) - changeX;
			int change = 10;
			int y = (int)(this.getY() + this.getDirection()/2 + h/2) - changeY;
			if (this.bWithBackground) {
				g.setColor(new Color(255, 255, 255, this.invisible));
				g.fillRect((int)(x - change), (int)(y - h - change), (int)(w + 2 * change), (int)(h + 2 * change));
				g.setColor(new Color(0, 0, 0, this.invisible));
				g.setStroke(new BasicStroke(3));
				g.drawRect((int)(x - change), (int)(y - h - change), (int)(w + 2 * change), (int)(h + 2 * change));
				g.setStroke(new BasicStroke(1));
			}
			g.setColor(new Color(0, 0, 0, this.invisible));
			g.drawString(s, x, y);
		}
	}
	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDirection() {
		return this.direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getColor() {
		return this.color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isVisible() {
		return bVisible;
	}

	public void setVisible(final boolean bVisible) {
		this.bVisible = bVisible;
	}
	
	public void think(final int delta) {
	}
	
	public void render(final Graphics2D g, final int changeX, final int changeY) {
		if (this.isVisible()) {
			
		}
	}

}