package apoGame;

public class ApoGamePanel extends ApoGameComponent {
	public void setEditor(boolean bSolvedLevel) {
		this.model = this.editor;
		
		this.setButtonVisible(ApoGameConstants.BUTTON_EDITOR);
		
		this.editor.setLevelSolved(bSolvedLevel);
		this.model.init();
	}
}