package org.apogames;

public class ApoGameConstants {
	/** indicates the direction "down" or direction south */
	public static final int PLAYER_DIRECTION_DOWN = 0;
	/** indicates the direction "left" or west */
	public static final int PLAYER_DIRECTION_LEFT = 1;
	/** indicates the direction "right" or east */
	public static final int PLAYER_DIRECTION_RIGHT = 2;
	/** indicates the direction "high" or north */
	public static final int PLAYER_DIRECTION_UP = 3;
	/** indicates the direction "high" or north */
	public static final int PLAYER_DIRECTION_CHANGEVISIBLE_UP = 4;
	/** indicates the direction "high" or north */
	public static final int PLAYER_DIRECTION_CHANGEVISIBLE_LEFT = 5;
	/** indicates that it is the finish */
	public static final int PLAYER_DIRECTION_FINISH = 6;
	/** indicates that it is the finish */
	public static final int PLAYER_DIRECTION_STEP = 7;
	/** indicates that it is the finish */
	public static final int PLAYER_DIRECTION_STEP_FINISH = 8;
	/** indicates the direction "NULL" */
	public static final int PLAYER_DIRECTION_NO_MOVEMENT = 9;
	
	/** Constant indicating what the minimum speed of the player is per millisecond */
	public static final float PLAYER_SPEED_MIN = 0.15f;
	/** Constant that indicates what the maximum speed of the player is per millisecond */
	public static final float PLAYER_SPEED_MAX = 0.15f;
}