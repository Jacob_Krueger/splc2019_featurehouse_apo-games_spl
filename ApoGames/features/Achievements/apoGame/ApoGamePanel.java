package apoGame;

import apoGame.game.ApoGameStateAchievements;

public class ApoGamePanel extends ApoGameComponent {
	private ApoGameStateAchievements achievements;
	
	public ApoGameStateAchievements getAchievements() {
		return this.achievements;
	}
	
	public void init() {
		original();
		if (this.achievements == null) {
			this.achievements = new ApoGameStateAchievements(this);
			this.achievements.loadAchievements();
		}
	}
}