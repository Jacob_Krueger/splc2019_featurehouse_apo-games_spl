# SPLC2019_FeatureHouse_Apo-Games_SPL

This repository comprises a FeatureIDE project that can simply be imported as new project. It uses FeatureHouse and provides a feature model and a set of implemented feature.

More details are explained in:

Jemel Debbiche, Oskar Lignell, Jacob Kürger, Thorsten Berger: Migrating Java-Based Apo-Games into a Composition-Based Software Product Line
